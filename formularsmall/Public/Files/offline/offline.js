(function() {

	if (queryString.birthDate) {
		var birthDateInt = parseInt(queryString.birthDate);
		if (!isNaN(birthDateInt)) {
			inca.form.env._FODELSEDATUM = RCC.Utils.ISO8601(new Date(birthDateInt));
		}
	}

	switch (queryString.formType) {
		case 'uppfoljning': RCC.Vast.Utils.Offline.changeRootTable('Uppfoljning', ['Behandling']); break;
		case 'behandling': RCC.Vast.Utils.Offline.changeRootTable('Behandling', []); break;
		default: RCC.Vast.Utils.Offline.changeRootTable('Behandling', []); break;
	}

	inca.form.env._SEX = (queryString.gender == 'man' ? 'M' : 'F');

	$(function () {
		$("body").prepend($('<button class="btn btn-primary">Skicka</button>').click(function ( ) { SendForm(); }));
	});
	
})();