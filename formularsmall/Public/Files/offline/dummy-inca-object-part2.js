(function ($, RCC, inca) {
	var lists =
	{
		JaNej:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' }
		],

		JaNejIU:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		JaNejKanske:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Kanske', value: 'kanske' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		JaNejVejEj:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Vej ej', value: 'vet_ej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		JaNejOkant:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Okänt', value: 'okant' }
		],

		Sjukhus: [
			{ text: 'Arvika', value: 'arvika' },
			{ text: 'Borås', value: 'boras' },
			{ text: 'Capio Psykiatri Allmänpsykiatri Jakobsberg', value: 'jakobsberg'},
			{ text: 'Capio Psykiatri Ångest och depression', value: 'angest_depression'},
			{ text: 'Danderyd', value: 'danderyd' },
			{ text: 'Eksjö', value: 'eksjo' },
			{ text: 'Eskilstuna', value: 'eskilstuna' },
			{ text: 'Falkenberg', value: 'falkenberg'},
			{ text: 'Falköping', value: 'falkoping' },
			{ text: 'Falun', value: 'falun' },
			{ text: 'Gällivare', value: 'gallivare' },
			{ text: 'Gävle', value: 'gavle' },
			{ text: 'Halmstad', value: 'halmstad' },
			{ text: 'Helsingborg', value: 'helsingborg' },
			{ text: 'Huddinge', value: 'huddinge' },
			{ text: 'Hudiksvall', value: 'hudiksvall' },
			{ text: 'Hässleholm', value: 'hassleholm' },
			{ text: 'Jönköping-Ryhov', value: 'jonkoping_ryhov' },
			{ text: 'Kalmar', value: 'kalmar' },
			{ text: 'Karlshamn', value: 'karlshamn' },
			{ text: 'Karlskoga', value: 'karlskoga' },
			{ text: 'Karlskrona', value: 'karlskrona' },
			{ text: 'Karlstad', value: 'karlstad' },
			{ text: 'Karolinska, Solna', value: 'karolinska_solna' },
			{ text: 'Katrineholm-Kullbergska', value: 'katrineholm_kullbergska' },
			{ text: 'Kungälv', value: 'kungalv' },
			{ text: 'Landskrona', value: 'landskrona' },
			{ text: 'Lindesberg', value: 'lindesberg' },
			{ text: 'Linköping', value: 'linkoping' },
			{ text: 'Ljungby', value: 'ljungby' },
			{ text: 'Lund', value: 'lund' },
			{ text: 'Löwenströmska', value: 'lowenstromska' },
			{ text: 'Malmö', value: 'malmo' },
			{ text: 'Malmö Psykiatriska Institut', value: 'malmo_psykiatriska_institut' },
			{ text: 'Mora', value: 'mora' },
			{ text: 'Motala', value: 'motala' },
			{ text: 'SU/Mölndal', value: 'su_molndal' },
			{ text: 'Norrköping', value: 'norrkoping' },
			{ text: 'Nyköping', value: 'nykoping' },
			{ text: 'Trollhättan NÄL', value: 'trollhattan_nal' },
			{ text: 'Piteå', value: 'pitea' },
			{ text: 'Prima Liljeholmen', value: 'prima_liljeholmen' },
			{ text: 'S:t Göran', value: 'st_goran' },
			{ text: 'SU/Östra sjukhuset', value: 'su_ostra_sjukhuset' },
			{ text: 'SU/Sahlgrenska', value: 'su_sahlgrenska' },
			{ text: 'Skellefteå', value: 'skelleftea' },
			{ text: 'Sollefteå', value: 'solleftea' },
			{ text: 'Sunderbyn', value: 'sunderbyn' },
			{ text: 'Sundsvall', value: 'sundsvall' },
			{ text: 'Säter', value: 'sater' },
			{ text: 'Södersjukhuset', value: 'sodersjukhuset' },
			{ text: 'Södertälje', value: 'sodertalje' },
			{ text: 'Trelleborg', value: 'trelleborg' },
			{ text: 'Uddevalla', value: 'uddevalla' },
			{ text: 'Umeå', value: 'umea' },
			{ text: 'Uppsala', value: 'uppsala' },
			{ text: 'Varberg', value: 'varberg' },
			{ text: 'WeMind Psykiatri, Haninge', value: 'haninge' },
			{ text: 'Visby', value: 'visby' },
			{ text: 'Värnamo', value: 'varnamo' },
			{ text: 'Västervik', value: 'vastervik' },
			{ text: 'Västerås', value: 'vasteras' },
			{ text: 'Växjö', value: 'vaxjo' },
			{ text: 'Ängelholm', value: 'angelholm' },
			{ text: 'Örebro', value: 'orebro' },
			{ text: 'Örnsköldsvik', value: 'ornskoldsvik' },
			{ text: 'Östersund', value: 'ostersund' },
		],
		
		Indikation:
		[
			{ text: 'Lindrig depressiv episod F320', value: 'F320' },
			{ text: 'Medelsvår depressiv episod F321', value: 'F321' },
			{ text: 'Svår depressiv episod utan psykotiska symptom F322', value: 'F322' },
			{ text: 'Recidiverande depression, lindrig episod F330', value: 'F330' },
			{ text: 'Recidiverande depression, medelsvår episod F331', value: 'F331' },
			{ text: 'Recidiverande depression, svår episod utan psykotiska symptom F332', value: 'F332' },
			{ text: 'Bipolär sjukdom, lindrig eller medelsvår depressiv episod F313', value: 'F313' },
			{ text: 'Bipolär sjukdom, svår depressiv episod utan psykotiska symptom F314', value: 'F314' },
			{ text: 'Postpartum depression F530', value: 'F530' },
			{ text: 'Organiskt förstämningssyndrom F063', value: 'F063' },
			{ text: 'Annan indikation, ICD-kod kan anges', value: 'annan' },
			{ text: 'Annan indikation fritext', value: 'fritext' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },

		],

		Sjukstatus:
		[
			{ text: 'Ej bedömt', value: 'ej_bedomt' },
			{ text: 'Normal, inte alls sjuk', value: 'normalt' },
			{ text: 'Gränsfall för psykisk sjukdom', value: 'gransfall' },
			{ text: 'Lindrigt sjuk', value: 'lindrigt' },
			{ text: 'Måttligt sjuk', value: 'mattligt' },
			{ text: 'Påtagligt sjuk', value: 'patagligt' },
			{ text: 'Allvarligt sjuk', value: 'allvarligt' },
			{ text: 'Bland de mest extremt sjuka patienterna', value: 'extremt' },
		],

		Suicidforsok:
		[
			{ text: 'Nej', value: 'nej' },
			{ text: '1-2 ggr', value: '1-2_ggr' },
			{ text: '3 ggr eller fler', value: 'ge_3_ggr' },
			{ text: 'Uppgift saknas, otillräcklig information', value: 'uppgift_saknas' }
		],

		JaNejMADRS:
		[
			{ text: 'Ja, svar på varje delfråga finns', value: 'ja' },
			{ text: 'Ja, endast totalpoäng kan anges', value: 'ja_totalpoang' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		JaNejMADRSS:
		[
			{ text: 'Ja, svar på varje delfråga finns', value: 'ja' },
			{ text: 'Ja, endast totalpoäng kan anges', value: 'ja_totalpoang' },
			{ text: 'Nej, patienten kunde pga sitt psykiska tillstånd ej genomföra självskattning', value: 'nej_psyk_tillstand' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		JaNejEQ5D:
		[
			{ text: 'Ja, svar på varje delfråga finns', value: 'ja' },
			{ text: 'Nej, patienten kunde pga sitt psykiska tillstånd ej genomföra självskattning', value: 'nej_psyk_tillstand' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		CPRS:
		[
			{ text: '0 - Jag upplever ingen minnesstörning', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 - Jag upplever tillfälliga minnesstörningar', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 - Jag upplever besvärande eller generande', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 -Jag upplever en total oförmåga att minnas överhuvudtaget', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		Rorlighet:
		[
			{ text: 'Jag går utan svårigheter', value: '1' },
			{ text: 'Jag kan gå men med viss svårighet', value: '2' },
			{ text: 'Jag är sängliggande', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Hygien:
		[
			{ text: 'Jag behöver ingen hjälp med min dagliga hygien, mat eller påklädning', value: '1' },
			{ text: 'Jag har vissa problem att tvätta eller klä mig själv', value: '2' },
			{ text: 'Jag kan inte tvätta eller klä mig själv ', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		HuduvudsakligaAktiviteter:
		[
			{ text: 'Jag klarar av mina huvudsakliga aktiviteter', value: '1' },
			{ text: 'Jag har vissa problem att klara av mina huvudsakliga aktiviteter', value: '2' },
			{ text: 'Jag klarar inte av mina huvudsakliga aktiviteter', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		SmartorBesvar:
		[
			{ text: 'Jag har varken smärtor eller besvär', value: '1' },
			{ text: 'Jag har måttliga smärtor eller besvär', value: '2' },
			{ text: 'Jag har svåra smärtor eller besvär', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		OroNedstamdhet:
		[
			{ text: 'Jag är inte orolig eller nedstämd', value: '1' },
			{ text: 'Jag är orolig eller nedstämd i viss utstäckning', value: '2' },
			{ text: 'Jag är i högsta grad orolig eller nedstämd', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Vardform:
		[
			{ text: 'Öppen vård', value: 'oppen' },
			{ text: 'Sluten vård', value: 'sluten' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Spoltyp:
		[
			{ text: '8-spole', value: '8_spole' },
			{ text: 'Vinklad 8-spole', value: 'vink_8_spole' },
			{ text: 'H-spole', value: 'h_spole' },
			{ text: 'Annan (specificera i fritext)', value: 'annan' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Stimulator:
		[
			{ text: 'Magventure R30', value: '1' },
			{ text: 'Magventure X100', value: '2' },
			{ text: 'Magstim', value: '3' },
			{ text: 'Mag & More', value: '4' },
			{ text: 'Annan apparat', value: 'annan' }
		],
		ThetaburstProtokoll:
		[
			{ text: 'iTBS', value: 'itbs' },
			{ text: 'cTBS', value: 'ctbs' },
			{ text: 'Annan (specificera i fritext)', value: 'annan' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		/*Pulsmonster:
		[
			{ text: 'Bifasisk', value: 'bifasisk' },
			{ text: 'Theta-burst', value: 'theta_burst' },
			{ text: 'Annat (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],*/
		Lokalisation:
		[
			{ text: 'Vä DLPFC', value: 'va_dlpfc' },
			{ text: 'Hö DLPFC', value: 'ho_dlpfc' },
			{ text: 'DMPFC', value: 'dmpfc' },
			{ text: 'Vä TPJ', value: 'va_tpj' },
			{ text: 'Annat område (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		LokalisationMetod:
		[
			{ text: '5-(6)cm regeln', value: '5cm_regeln' },
			{ text: '10-20 systemet', value: '10_20_systemet' },
			{ text: 'Neuronavigation', value: 'neuronavigation' },
			{ text: 'Annat (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		Motortroskelmetod:
		[
			{ text: 'EMG', value: 'emg' },
			{ text: 'Okulär besiktning hand', value: 'okular_besiktning_hand' },
			{ text: 'Okulär besiktning fot', value: 'okular_besiktning_fot' },
			{ text: 'Annat område (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		BiverkanKomplikation:
		[
			{ text: 'Smärta under stimulering', value: 'smarta_stimulering' },
			{ text: 'Annan huvudvärk', value: 'huvudvärk' },
			{ text: 'Svimning', value: 'svimning' },
			{ text: 'Kramp', value: 'kramp' },
			{ text: 'Yrsel', value: 'yrsel' },
			{ text: 'Muskelryckningar', value: 'muskelryckningar' },
			{ text: 'Ångest', value: 'angest' },
			{ text: 'Annan biverkan/komplikation', value: 'annan' },
		],
		OrsakAvslut:
		[
			{ text: 'Remission', value: 'remission' },
			{ text: 'Remission bedöms ej möjlig trots viss effekt', value: 'remission_bed_ej' },
			{ text: 'Brist på för behandlingen nödvändiga resurser', value: 'brist_beh_resurs' },
			{ text: 'Utebliven effekt', value: 'utebliven_effekt' },
			{ text: 'Biverkning', value: 'biverkning' },
			{ text: 'Ej avslutad', value: 'ej_avslutad' },
			{ text: 'Annan, vilken?', value: 'annan' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		KliniskBedomning:
		[
			{ text: 'Ej bedömd', value: 'ej_bedomd' },
			{ text: 'Väldigt mycket förbättrad', value: 'vald_mkt_forbattrad' },
			{ text: 'Mycket förbättrad', value: 'mkt_forbattrad' },
			{ text: 'Minimalt förbättrad', value: 'min_forbattrad' },
			{ text: 'Oförändrad', value: 'oforandrad' },
			{ text: 'Minimalt försämrad', value: 'min_forsamrad' },
			{ text: 'Mycket försämrad', value: 'mkt_forsamrad' },
			{ text: 'Väldigt mycket försämrad', value: 'vald_mkt_forsamrad' },
		],
		InfoMetod:
		[
			{ text: 'Skriftligt och muntligt', value: 'skriftligt_muntligt' },
			{ text: 'Skriftligt', value: 'skriftligt' },
			{ text: 'Mutligt', value: 'muntligt' },
			{ text: 'Inte alls', value: 'inte_alls' },
			{ text: 'Vet ej', value: 'vet_ej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		BesvarLangd:
		[
			{ text: 'Mindre än en månad', value: 'manad_mindre' },
			{ text: 'Mer än en månad, men besväret har gått över', value: 'manad_mer' },
			{ text: 'Jag har fortfarande besväret', value: 'fort_besvar' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Behandlingar6man:
		[
			{ text: 'Antidepressiva läkemedel', value: 'antidep' },
			{ text: 'Litium', value: 'litium' },
			{ text: 'Kognitiv beteendeterapi', value: 'kogn_terapi' },
			{ text: 'Annan psykologisk behandling', value: 'annan_psyk_beh' },
			{ text: 'Ingen av dessa', value: 'ingen' },
		],

		VardasFrivilligt: [
			{ text: 'Frivillig vård', 'value': 'frivillig_vard' },
			{ text: 'LPT', 'value': 'lpt' },
			{ text: 'LRV', 'value': 'lrv' },
			{ text: 'Uppgift saknas', 'value': 'uppgift_saknas' }
		],


		EgenSinnesstämning: [
			{ text: '0 Jag kan känna mig glad eller ledsen, allt efter omständigheterna.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag känner mig nedstämd för det mesta, men ibland kan det kännas lättare.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag känner mig genomgående nedstämd och dyster. Jag kan inte glädja mig (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag är totalt nedstämd och olycklig att jag inte kan tänka mig värre.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgnaOroskänslor: [
			{ text: '0 Jag känner mig mestadels lugn.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Ibland har jag obehagliga känslor av inre oro.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag har ofta en känsla av inre oro som ibland kan bli mycket stark, och som (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag har fruktansvärda, långvariga eller outhärdliga ångestkänslor.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenSömn: [
			{ text: '0 Jag sover lugnt och bra och tillräckligt länge för mina behov. Jag har inga (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag har vissa sömnsvårigheter. Ibland har jag svårt att somna eller sover (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag sover minst två timmar mindre per natt än normalt. Jag vaknar ofta (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag sover mycket dåligt, inte mer än 2-3 timmar per natt.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenMatlust: [
			{ text: '0 Min aptit är som den brukar vara.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Min aptit är sämre än vanligt.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Min aptit har nästan helt försvunnit. Maten smakar inte och jag måste (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag vill inte ha någon mat. Om jag skall få någonting i mig, måste jag (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenKoncentrationsförmåga: [
			{ text: '0 Jag har inga koncentrationssvårigheter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag har tillfälligt svårt att hålla tankarna samlade på sådant som normalt (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag har påtagligt svårt att koncentrera mig på sådant som normalt inte (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag kan överhuvudtaget inte koncentrera mig på någonting.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenInitiativförmåga: [
			{ text: '0 Jag har inga svårigheter med att ta itu med nya uppgifter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 När jag skall ta itu med något, tar det emot på ett sätt som inte är (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Det krävs en stor ansträngning för mig att ens komma igång med enkla (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag kan inte förmå mig att ta itu med de enklaste vardagssysslor.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgetKänslomässigtEngagemang: [
			{ text: '0 Jag är intresserad av omvärlden och engagerar mig i den, och det bereder (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag känner mindre starkt för sådant som brukar engagera mig. Jag har (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag kan inte känna något intresse för omvärlden, inte ens för vänner (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag har slutat uppleva några känslor. Jag känner mig smärtsamt likgiltig (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenPessimism: [
			{ text: '0 Jag ser på framtiden med tillförsikt. Jag är på det hela taget ganska nöjd (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Ibland klandrar jag mig själv och tycker att jag är mindre värd än andra.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag grubblar ofta över mina misslyckanden och känner mig mindervärdig (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag ser allting i svart och kan inte se någon ljusning. Det känns som (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenLivslust: [
			{ text: '0 Jag har normal aptit på livet.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Livet känns inte särskilt meningsfullt men jag önskar ändå inte att jag (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag tycker ofta det vore bättre att vara död, och trots att jag egentligen (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag är egentligen övertygad om att min enda utväg är att dö (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgetMinne: [
			{ text: '0 - Jag upplever ingen minnesstörning', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 - Jag upplever tillfälliga minnesstörningar', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 - Jag upplever besvärande eller generande minnesstörningar', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 - Jag upplever en total oförmåga att minnas överhuvud taget', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],



		Nedstämdhet: [
			{ text: '0 Neutralt stämningsläge. Kan känna såväl tillfällig munterhet som (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Övervägande upplevelser av nedstämdhet men ljusare stunder förekommer.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Genomgående nedstämdhet och dyster till sinnes. Sinnesstämningen påverkas (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Genomgående upplevelser av maximal nedstämdhet.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }

		],

		SänktGrundstämning: [
			{ text: '0 Neutral stämningsläge.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Ser genomgående nedstämd ut, men kan tillfälligt växla till ljusare (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ser nedstämd och olycklig ut oavsett samtalsämne.', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Genomgående uttryck för extrem dysterhet, tungsinne eller förtvivlad olycka.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Ångestkänslor: [
			{ text: '0 Mestadels lugn.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Tillfälliga känslor av obehaglig psykisk spänning.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ständig känsla av inre oro, någon gång så intensiv att den endast med (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Långdragna ångestkänslor. Överväldigande känslor av skräck eller dödsångest (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		MinskadNattsömn: [
			{ text: '0 Sover som vanligt.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Måttliga insomningssvårigheter eller kortare, ytligare eller oroligare sömn (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Minskad sömntid (minst två timmar mindre än normalt). Vaknar ofta (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Mindre än två till tre timmars nattsömn totalt.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		MinskadAptit: [
			{ text: '0 Normalt eller ökad aptit.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Dålig matlust.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Aptit saknas nästan helt, maten smakar inte, måste tvinga sig att äta.', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Måste övertalas att äta något överhuvudtaget. Matvägran.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Koncentrationssvårigheter: [
			{ text: '0 Inga koncentrationssvårigheter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Tillfälligt svårt att hålla tankarna samlade vid t.ex. läsning eller TV-tittande.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Uppenbara koncentrationssvårigheter som försvårar läsning eller samtal.', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Kontinuerliga, invalidiserande koncentrationssvårigheter.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Initiativlöshet: [
			{ text: '0 Ingen svårighet att ta itu med nya uppgifter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Lätta igångsättningssvårigheter.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Svårt att komma igång även med enkla rutinuppgifter, som kräver (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Oförmögen att ta initiativ till de enklaste aktiviteter. Kan inte påbörja (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		KänslomässigtEngagemang: [
			{ text: '0 Normalt intresse för omvärlden och för andra människor.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Svårigheter att finna nöje i sådant som vanligen väcker intresse. Minskad (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ointresserad av omvärlden. Upplevelser av likgiltighet inför vänner (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Total oförmåga att känna adekvat sorg eller vrede. Totalt eller smärtsam (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		DepressivtTankeinnehåll: [
			{ text: '0 Inga pessimistiska tankar.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Fluktuerande självförebråelser och mindervärdesidéer.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ständiga självanklagelser. Klara, men inte orimliga, tankar om synd (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Absurda föreställningar om ekonomisk ruin och oförlåtliga synder. Absurda (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		LivsledaSjälvmordstankar: [
			{ text: '0 Ordinär livslust. Inga självmordstankar.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Livsleda, men inga eller endast vaga dödsönskningar.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Självmordstankar förekommer och självmord betraktas som en tänkbar utväg (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Uttalande avsikter att begå självmord, när tillfälle bjuds. Aktiva (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
	};

	// Add dummy ids.
	var n = 0;
	$.each(lists,
		function (unused, list) {
			n++;
			$.each(list,
				function (i, opt) {
					if (!opt.hasOwnProperty('id'))
						opt.id = n * 10000 + i;
				});
		});

	var q = function (x, y) { return x; };

	$.extend(true, inca.form, RCC.Utils.createDummyForm(
	{
		rootTable: 'Behandling',
		
		regvars:
		{
			datifylld: q('datetime', 'NY'),
			enhet: q('integer', 'NY'),
			sjukhus: q(lists.Sjukhus, 'Ny'),
			r_indikation: q(lists.Indikation, 'NY'),
			foreCGI: q(lists.Sjukstatus, 'NY'),
			suicidforsok: q(lists.Suicidforsok, 'NY'),
			foreMADRS: q(lists.JaNejMADRS, 'NY'),
			foreMADRSS: q(lists.JaNejMADRSS, 'NY'),
			foreEQ5D: q(lists.JaNejEQ5D, 'NY'),
			foreMinnesstorning: q(lists.CPRS, 'NY'),
			vardform: q(lists.Vardform, 'NY'),
			vardasFrivilligt: q(lists.VardasFrivilligt, 'NY'),
			tidigareRTMS: q(lists.JaNejIU, 'NY'),
			tidigareECT: q(lists.JaNejIU, 'NY'),
			forstaBehTillfalle: q('datetime', 'NY'),
			sistaBehTillfalle: q('datetime', 'NY'),
			dummy: q('datetime', 'NY'),
			antalBehTillfalle: q('integer', 'NY'),
			spoltyp: q(lists.Spoltyp, 'NY'),
			stimulator: q(lists.Stimulator, 'NY'),
			//pulsmonster: q(lists.Pulsmonster, 'NY'),
			thetaburstProtokoll: q(lists.ThetaburstProtokoll, 'NY'),
			lokalisation: q(lists.Lokalisation, 'NY'),
			lokalisationMetod: q(lists.LokalisationMetod, 'NY'),
			motortroskelMetod: q(lists.Motortroskelmetod, 'NY'),
			motortroskel: q('integer', 'NY'),
			titrering: q(lists.JaNejIU, 'NY'),
			sistaStyrka: q('integer', 'NY'),
			dosFrekvens: q('integer', 'NY'),
			dosPuls: q('integer', 'NY'),
			dosTidPulstag: q('integer', 'NY'),
			dosAntalPulstag: q('integer', 'NY'),
			dosTotAntalPulser: q('integer', 'NY'),
			dosTotBehDuration: q('integer', 'NY'),
			dosTotBehDurationSekunder: q('integer', 'NY'),
			antiDep: q(lists.JaNejIU, 'NY'),
			litium: q(lists.JaNejIU, 'NY'),
			lamotrigin: q(lists.JaNejIU, 'NY'),
			valproat: q(lists.JaNejIU, 'NY'),
			bensodiazepin: q(lists.JaNejIU, 'NY'),
			antiEp: q(lists.JaNejIU, 'NY'),
			antiPsyk: q(lists.JaNejIU, 'NY'),
			planFortsattRTMS: q(lists.JaNejIU, 'NY'),
			planECT: q(lists.JaNejIU, 'NY'),
			planSysPsykBeh: q(lists.JaNejIU, 'NY'),
			biverkanKomplikation: q(lists.JaNejIU, 'NY'),
			orsakAvslut: q(lists.OrsakAvslut, 'NY'),
			kliniskBedomning: q(lists.KliniskBedomning, 'NY'),
			efterMinnesstorning: q(lists.CPRS, 'NY'),
			efterMADRS: q(lists.JaNejMADRS, 'NY'),
			efterMADRSS: q(lists.JaNejMADRSS, 'NY'),
			efterEQ5D: q(lists.JaNejEQ5D, 'NY'),
			rtmsIgen: q(lists.JaNejKanske, 'NY'),
			annanInfo: q(lists.JaNej, 'NY'),
			efterCGI: q(lists.Sjukstatus, 'NY'),
		},
		subTables:
		{
			Behandlingsnummer:
			{
				regvars:
				{
				   behandlingsnummer: q('integer', 'NY'),
				},
				vdlists: { },
				subTables: {} 
			},
			Uppfoljning:
			{
				regvars:
				{
					uppfDatifylld: q('datetime', 'NY'),
					patientenkatBesvarad: q(lists.JaNej, 'NY'),
					kvarstaendeBiverkan: q(lists.JaNejOkant, 'NY'),
				},
				vdlists: { rtms_behandlingar: 'vd' },
				subTables:
				{
					Patientenkat:
					{
						regvars:
						{
						   	patSvarDatRTMS: q('datetime', 'NY'),
						   	tillrackligInfo: q(lists.JaNejVejEj, 'NY'),
							infoMetod: q(lists.InfoMetod, 'NY'),
							hjalpteRTMS: q(lists.JaNejKanske, 'NY'),
							besvarRTMS: q(lists.JaNejVejEj, 'NY'),
							rtmsIgenUppf: q(lists.JaNejKanske, 'NY'),
							fragorKontaktEnhet: q(lists.JaNejIU, 'NY'),
							ovrigKommentar: q('string', 'NY'),
							uppfMinnesstorning: q(lists.CPRS, 'NY'),
							uppfMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
							uppfMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
							uppfMADRSSSomn: q(lists.EgenSömn, 'NY'),						
							uppfMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
							uppfMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
							uppfMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
							uppfMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
							uppfMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
							uppfMADRSSLivslust: q(lists.EgenLivslust, 'NY'),
							uppfMADRSSTotalpoang: q('integer', 'NY'),
							uppfEQ5DHalsotillstand: q('integer', 'NY'),
							uppfEQ5DRorlighet: q(lists.Rorlighet, 'NY'),
							uppfEQ5DHygien: q(lists.Hygien, 'NY'),
							uppfEQ5DHuvudsakAktiviteter: q(lists.HuduvudsakligaAktiviteter, 'NY'),
							uppfEQ5DSmartorBesvar: q(lists.SmartorBesvar, 'NY'),
							uppfEQ5DOroNedstamdhet: q(lists.OroNedstamdhet, 'NY')
						},
						vdlists: { },
						subTables: {
							Besvar:
							{
								regvars:
								{
								   besvarFritext: q('string', 'NY'),
								   besvarLangd: q(lists.BesvarLangd, 'NY'),
								},
								vdlists: { },
								subTables: {} 
							},

							Behandlingar6man:
							{
								regvars:
								{
								   behandlingar6man: q(lists.Behandlingar6man, 'NY')
								},
								vdlists: { },
								subTables: {} 
							},
						} 
					},

					KvarstaendeBiverkan:
					{
						regvars:
						{
							kvarstaendBiverkanFritext: q('string', 'NY')
						},
						vdlists: { },
						subTables: {} 
					}

				}
			},
			UnderCGI:
			{
				regvars:
				{
				   underCGI: q(lists.Sjukstatus, 'NY')
				},
				vdlists: { },
				subTables: {
					UnderCGISkattning:
					{
						regvars:
						{
						   underCGISkattning: q('datetime', 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},
			BehandlingsTillfalle:
			{
				regvars:
				{
				   behTillfalle: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			IndikationAnnan:
			{
				regvars:
				{
				   r_indikationAnnan: q('vd', 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			IndikationFritext:
			{
				regvars:
				{
				   r_indikationFritext: q('string', 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			Suicidforsok12man:
			{
				regvars:
				{
				   suicidforsok12man: q(lists.Suicidforsok, 'NY')
				},
				vdlists: { },
				subTables: {} 
			},

			ForeMADRSTotalpoang:
			{
				regvars:
				{
				   foreMADRSTotalpoang: q('integer', 'NY'),
				   foreMADRSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					ForeMADRSDelsvar:
					{
						regvars:
						{
							foreMADRSNedstamdhet: q(lists.Nedstämdhet, 'NY'),			
							foreMADRSSAnktGrundstamning: q(lists.SänktGrundstämning, 'NY'),							
							foreMADRSAngestkanslor: q(lists.Ångestkänslor, 'NY'),						
							foreMADRSMinskadNattsomn: q(lists.MinskadNattsömn, 'NY'),							
							foreMADRSMinskadAptit: q(lists.MinskadAptit, 'NY'),						
							foreMADRSKoncsvarigheter: q(lists.Koncentrationssvårigheter, 'NY'),							
							foreMADRSInitiativloshet: q(lists.Initiativlöshet, 'NY'),						
							foreMADRSKanslomassigtEng: q(lists.KänslomässigtEngagemang, 'NY'),					
							foreMADRSDepTankeinnehall: q(lists.DepressivtTankeinnehåll, 'NY'),							
							foreMADRSSjalvmordstankar: q(lists.LivsledaSjälvmordstankar, 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},
			
			ForeMADRSSTotalpoang:
			{
				regvars:
				{
				   foreMADRSSTotalpoang: q('integer', 'NY'),
				   foreMADRSSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					ForeMADRSSDelsvar:
					{
						regvars:
						{
							foreMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
							foreMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
							foreMADRSSSomn: q(lists.EgenSömn, 'NY'),						
							foreMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
							foreMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
							foreMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
							foreMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
							foreMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
							foreMADRSSLivslust: q(lists.EgenLivslust, 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},

			ForeEQ5D:
			{
				regvars:
				{
					foreEQ5DHalsotillstand: q('integer', 'NY'),
				    foreEQ5DDatum: q('datetime', 'NY'),
					foreEQ5DRorlighet: q(lists.Rorlighet, 'NY'),
					foreEQ5DHygien: q(lists.Hygien, 'NY'),
					foreEQ5DHuvudsakAktiviteter: q(lists.HuduvudsakligaAktiviteter, 'NY'),
					foreEQ5DSmartorBesvar: q(lists.SmartorBesvar, 'NY'),
					foreEQ5DOroNedstamdhet: q(lists.OroNedstamdhet, 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			UnderMADRSS:
			{
				regvars:
				{
				   underMADRSS: q(lists.JaNejMADRSS, 'NY'),
				},
				vdlists: { },
				subTables: {
					UnderMADRSSTotalpoang:
					{
						regvars:
						{
						   underMADRSSTotalpoang: q('integer', 'NY'),
						   underMADRSSDatum: q('datetime', 'NY')
						},
						vdlists: { },
						subTables: {
							UnderMADRSSDelsvar:
							{
								regvars:
								{
									underMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
									underMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
									underMADRSSSomn: q(lists.EgenSömn, 'NY'),						
									underMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
									underMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
									underMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
									underMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
									underMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
									underMADRSSLivslust: q(lists.EgenLivslust, 'NY'),
								},
								vdlists: { },
								subTables: {
									
								} 
							}
						} 
					},
					
				} 
			},
			
			SpoltypAnnat:
			{
				regvars:
				{
					spoltypAnnat: q('string', 'NY')
				}	,			
				vdlists: { },
				subTables: {} 
			},
			StimulatorAnnan:
			{
				regvars:
				{
					stimulatorAnnan: q('string', 'NY')
				}	,			
				vdlists: { },
				subTables: {} 
			},
			ThetaburstProtokollAnnan:
			{
				regvars:
				{
					thetaburstProtokollAnnan: q('string', 'NY'),
				},			
				vdlists: { },
				subTables: {} 
			},
			LokalisationAnnat:
			{
				regvars:
				{
					lokalisationAnnat: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {} 
			},
			LokalisationMetodAnnat:
			{
				regvars:
				{
					lokalisationMetodAnnat: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {} 
			},
			MotortroskelMetodAnnat:
			{
				regvars:
				{
					motortroskelMetodAnnat: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {} 
			},
			BiverkanKomplikation:
			{
				regvars:
				{
					biverkanKomplikationLista: q(lists.BiverkanKomplikation, 'NY'),
				},				
				vdlists: { },
				subTables: {
					BiverkanKomplikationAnnan:
					{
						regvars:
						{
							biverkanKomplikationAnnan: q('string', 'NY'),
						},				
						vdlists: { },
						subTables: {
							
						} 
					}
				} 
			},
			OrsakAvslutAnnan:
			{
				regvars:
				{
					orsakAvslutAnnan: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {
					
				} 
			},

			EfterMADRSTotalpoang:
			{
				regvars:
				{
				   efterMADRSTotalpoang: q('integer', 'NY'),
				   efterMADRSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					EfterMADRSDelsvar:
					{
						regvars:
						{
							efterMADRSNedstamdhet: q(lists.Nedstämdhet, 'NY'),			
							efterMADRSSAnktGrundstamning: q(lists.SänktGrundstämning, 'NY'),							
							efterMADRSAngestkanslor: q(lists.Ångestkänslor, 'NY'),						
							efterMADRSMinskadNattsomn: q(lists.MinskadNattsömn, 'NY'),							
							efterMADRSMinskadAptit: q(lists.MinskadAptit, 'NY'),						
							efterMADRSKoncsvarigheter: q(lists.Koncentrationssvårigheter, 'NY'),							
							efterMADRSInitiativloshet: q(lists.Initiativlöshet, 'NY'),						
							efterMADRSKanslomassigtEng: q(lists.KänslomässigtEngagemang, 'NY'),					
							efterMADRSDepTankeinnehall: q(lists.DepressivtTankeinnehåll, 'NY'),							
							efterMADRSSjalvmordstankar: q(lists.LivsledaSjälvmordstankar, 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},
			
			EfterMADRSSTotalpoang:
			{
				regvars:
				{
				   efterMADRSSTotalpoang: q('integer', 'NY'),
				   efterMADRSSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					EfterMADRSSDelsvar:
					{
						regvars:
						{
							efterMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
							efterMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
							efterMADRSSSomn: q(lists.EgenSömn, 'NY'),						
							efterMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
							efterMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
							efterMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
							efterMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
							efterMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
							efterMADRSSLivslust: q(lists.EgenLivslust, 'NY')
						},
						vdlists: { },
						subTables: {} 
					}
				} 
			},

			EfterEQ5D:
			{
				regvars:
				{
					efterEQ5DHalsotillstand: q('integer', 'NY'),
				    efterEQ5DDatum: q('datetime', 'NY'),
					efterEQ5DRorlighet: q(lists.Rorlighet, 'NY'),
					efterEQ5DHygien: q(lists.Hygien, 'NY'),
					efterEQ5DHuvudsakAktiviteter: q(lists.HuduvudsakligaAktiviteter, 'NY'),
					efterEQ5DSmartorBesvar: q(lists.SmartorBesvar, 'NY'),
					efterEQ5DOroNedstamdhet: q(lists.OroNedstamdhet, 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			AnnanInfo:
			{
				regvars:
				{
				   annanInfoFritext: q('string', 'NY'),
				},
				vdlists: { },
				subTables: {} 
			}	
		}
	}));

})(window['jQuery'], window['RCC'], window['inca']);
