window.inca =
{
	user: { role: { isReviewer: false }, region: { id: -1, name: 'Väst' }, position: {id: 1277, name: "Örebro", code: "1", fullNameWithCode: "Region Örebro (18) - Örebro läns landsting (ECT-1) - Örebro (1)"} },
	on: function ( ) { if ( window.console ) console.warn( 'inca.on not implemented' ); },
	form:
	{
		isReadOnly: false,
		getRegisterRecordData:
			function ( )
			{
				throw new Error('unimplemented');
			},
		getValueDomainValues:
			function ( opt )
			{
				if ( opt.success )
				{
					setTimeout(
						function ( )
						{
							switch ( opt.vdlist )
							{
								case 'VD_ICD10_Kapitel':
									opt.success(recording.getValueDomainValues["[[\"error\",null],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitel\"]]"][0]);
									break;
								case 'VD_ICD10_Kapitelkoder':
									opt.success(recording.getValueDomainValues["[[\"error\",null],[\"parameters\",[[\"kapitel\"," + opt.parameters.kapitel + "]]],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitelkoder\"]]"][0]);
									break;
								case 'VD_rtms_behandlingar':
									opt.success( [{"text":"x","id":4,"data":{"sistaBehTillfalle":"2014-01-05"}},{"text":"x","id":49,"data":{"sistaBehTillfalle":"2015-11-05"}}] );
									break;
							}
						}, (1+Math.random())*250 ); 
				}
			}
	},
	errand: { status: { val: function () { return 'Nytt ärende'; } } },
	serverDate: new Date()
};
