    /**
    * 
    * @param {Object} opt 
    * @param {ko.observableArray} opt.errorsTo Mål för eventuella felmeddelanden
    * @param {String} opt.valueDomain Namn på värdedomän, anropas med patientId som parameter
    * @param {Function} opt.getError Funktion som anropas med värdedomänsvärdena som enda argument och returnerar eventuellt felmeddelande
    * 
    */

    /*
    Exempel på användning:

    ECT:
    
	new DoubleRegistrationCheck({
		errorsTo: ,
		valueDomain: '',
		getError: function(list) {
			var doubleRegistration = _.find(list, function(obj){ return vm.DatForstaECT() == obj.data.DatForstaECT 
				|| vm.DatSistaECT() == obj.data.DatSistaECT ; });

			if ( doubleRegistration )
				return 'Varning! Det finns redan en behandling registrerad med samma datum.';
		}	
	})
    
    Nyregistrering:
    
	new DoubleRegistrationCheck({
		errorsTo: ,
		valueDomain: '',
		getError: function(list) {
			if ( list.length > 0 )
				return 'Varning!';
		}	
	})
    
    Uppföljning:
    
	new DoubleRegistrationCheck({
		errorsTo: ,
		valueDomain: '',
		getError: function(list) {
			
			var days = 30;
			
			var doubleRegistration = _.find(list, function(obj){ 
				return Math.abs( RCC.Utils.parseYMD(vm.informationsdatum()) - RCC.Utils.parseYMD(obj.data.informationsdatum)) <=
				(days * 24 * 60 * 60 * 1000) ; });

			if ( doubleRegistration )
				return 'Varning!';

		}	
	})
    */


function DoubleRegistrationCheck( opt ) {

	var self = this;
	self.errorObj = ko.observable(undefined);
	self.errorsTo = opt.errorsTo;
	self.valueDomainValues = ko.observableArray();
	self.valueDomainValues(undefined);
	self.getError = opt.getError;

	ko.computed(self._checkForErrors, self); 

	self.loadValueDomain(opt.valueDomain);

}

DoubleRegistrationCheck.prototype._checkForErrors = 
function() {

	var list = this.valueDomainValues();

	if (!list)
		return;

	this.errorsTo.remove(this.errorObj);

	var error = this.getError(list);

	if (error) {
		this.errorObj(error);
		this.errorsTo.push(this.errorObj);
	}

}

DoubleRegistrationCheck.prototype.loadValueDomain =
function( valueDomain ){
	
	var self = this;

	inca.form.getValueDomainValues(
		{
			vdlist: valueDomain,
			parameters: { patId: inca.form.env._PATIENTID },
			success:
				function (list)
				{
					self.valueDomainValues(list);
				},
			error:
				function (error)
				{
					self.errorsTo.push('Lyckades inte ladda värdedomänen ' + valueDomain + ': ' + error);
				}
		});
}
