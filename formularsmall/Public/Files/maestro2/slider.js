/**
* Slider
* Användning:
*	<m-slider params="@param: värde"></m-slider>
*
* @param {ko.observable} value Datakoppling.
* @param {Number} [min=0] Minsta tillåtna värde.
* @param {Number} [max=100] Största tillåtna värde.
* @param {Boolean} [readOnly=false] Tillåt endast visning av värdet.
*/
(function ($, _, RCC, ko) {
	ko.components.register('m-slider', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.value) throw new Error('Parameter "value" is required.');

			var self = this, defaults = { value: undefined, min: 0, max: 100, readOnly: false };
			_.defaults(self, _.pick(params, _.keys(defaults)), defaults);

			self.refreshOnHover = false;
			self.event = (self.readOnly ? {} : {
				mouseleave: function() { self.refreshOnHover = false; },
				onselectstart: function() { return false; }
			});
			
			self.sliderValues = (function(obsValue, step) {
				function SliderValue(value) {
					var me = this;
					me.value = value * step;
					me.selected = ko.observable(value <= obsValue);
					me.event = (self.readOnly ? {} : {
						mousedown: function() { self.refreshOnHover = true; self.value(me.value); },
						mouseover: function() { if (self.refreshOnHover) self.value(me.value); },
						mouseup: function() { self.refreshOnHover = false; }
					});
				}

				return _.map(_.range(100, -1, -1), function(value) { return new SliderValue(value); });

			})(+self.value(), (self.max - self.min) / 100);
			
			self.value.subscribe(function () {
				var obsValue = +self.value();
				_.each(self.sliderValues, function (o) {
					o.selected(o.value <= obsValue);
				});
			});
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div class="rcc-slider" data-bind="css: { 'rcc-slider-readonly': readOnly }">
				<div class="inner" data-bind="foreach: sliderValues, event: event">
					<div data-bind="event: event, css: { selected: selected }"></div>
				</div>
			</div>
		*/return undefined;})
	});
})(jQuery, _, RCC, ko);