/**
* @param {String} [label] Label fråga och modaltitle.
* @param {Object} [regvar] Registervariabel.
* @param {Object[]} [data] Integrerad manual.
*/

(function () {
    ko.components.register('modal-picker', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function(params) {
            var self = this;
            self.data = params.data;
            self.title = params.title;
            self.label = params.label;
            self.displayText = ko.observable();
            self.regvar = params.regvar;
            self.showDialog = ko.observable(false);
            
            self.setValueAndText = function(obj) {
                self.displayText(obj.text);
                self.regvar(ko.utils.arrayFirst(self.regvar.rcc.term.listValues, function (list) { 
                    return list.text === obj.text;
                }));
                self.showDialog(false);
            };


            if(self.regvar() && self.data.length) {
                self.setValueAndText(_.find(self.data, function(obj) {
                    return obj.text == self.regvar().text;
                }));
            }

            self.hasErrors = ko.computed(function () {
                var errors = self.regvar && self.regvar.rcc && self.regvar.rcc.validation && self.regvar.rcc.validation.errors();
                return errors && errors.length;
            });

             self.hasFatalErrors = ko.computed(function () {
                var errors = self.regvar && self.regvar.rcc && self.regvar.rcc.validation && self.regvar.rcc.validation.errors();
                return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
            });

        },
        template: RCC.Vast.Utils.extractSource(function() {/**@preserve
            <div class="single-answer-template r">
                <label data-bind="css: { 'accessed': regvar.rcc && regvar.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
                    <span class="question" data-bind="text: label"></span>
                    <span data-bind="text: displayText"></span>
                    <button class="btn btn-primary btn-xs" style="margin-left: 3px" data-bind="click: function() { showDialog(true); }">Välj</button>
                    <div class="error-list" data-bind="foreach: regvar.rcc.validation.errors">
                        <span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
                            <span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
                            <span data-bind="text: message"></span>
                        </span>
                    </div>
                    <!-- ko if: showDialog -->
                        <div class="rcc-modal">
                            <div class="rcc-modal-overlay"></div> 
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button data-bind="click: function() { showDialog(false); }" class="close">&times;</button>
                                        <h4 class="modal-title" data-bind="text: label"></h4>
                                    </div>
                                    <div class="modal-body">
                                       <table class="table table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Värde</th>
                                                    <th style="width: 80%">Beskrivning</th>
                                                    <th style="width: 10%">Åtgärd</th>
                                                </tr>
                                            </thead>
                                            <tbody data-bind="foreach: data">
                                                <tr>
                                                    <td data-bind="html: text"></td>
                                                    <td data-bind="html: infoText"></td>
                                                    <td><button class="btn btn-primary btn-xs" data-bind="click: $parent.setValueAndText">Välj</button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!--/ko -->  
                </label>
            </div>  
        */return undefined;})
    });
})();
