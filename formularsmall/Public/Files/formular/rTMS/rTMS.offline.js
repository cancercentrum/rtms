/*! rTMS | 2021-12-09 12:20:18 */
window.inca =
{
	user: { role: { isReviewer: false }, region: { id: -1, name: 'Väst' }, position: {id: 1277, name: "Örebro", code: "1", fullNameWithCode: "Region Örebro (18) - Örebro läns landsting (ECT-1) - Örebro (1)"} },
	on: function ( ) { if ( window.console ) console.warn( 'inca.on not implemented' ); },
	form:
	{
		isReadOnly: false,
		getRegisterRecordData:
			function ( )
			{
				throw new Error('unimplemented');
			},
		getValueDomainValues:
			function ( opt )
			{
				if ( opt.success )
				{
					setTimeout(
						function ( )
						{
							switch ( opt.vdlist )
							{
								case 'VD_ICD10_Kapitel':
									opt.success(recording.getValueDomainValues["[[\"error\",null],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitel\"]]"][0]);
									break;
								case 'VD_ICD10_Kapitelkoder':
									opt.success(recording.getValueDomainValues["[[\"error\",null],[\"parameters\",[[\"kapitel\"," + opt.parameters.kapitel + "]]],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitelkoder\"]]"][0]);
									break;
								case 'VD_rtms_behandlingar':
									opt.success( [{"text":"x","id":4,"data":{"sistaBehTillfalle":"2014-01-05"}},{"text":"x","id":49,"data":{"sistaBehTillfalle":"2015-11-05"}}] );
									break;
							}
						}, (1+Math.random())*250 ); 
				}
			}
	},
	errand: { status: { val: function () { return 'Nytt ärende'; } } },
	serverDate: new Date()
};

(function(f){f.RCC={metadataProperty:"rcc",bindingsPrefix:"rcc-"}})(window);(function(f,d,b){var a=b.bindingsPrefix+"included";b.BindingHandlers={};b.BindingHandlers.nearestVariable=function(b,a){for(var c=[].concat(a.$data,a.$parents,a.$root),d=0;d<c.length;d++)if(c[d].hasOwnProperty(b))return c[d][b]};var c=b.bindingsPrefix+"var";d.bindingHandlers[c]={init:function(c,h,g,p,l){var j=h();if(!j[b.metadataProperty])throw Error('The binding "'+b.bindingsPrefix+'var" requires an observable from RCC.ViewModel as argument.');"checkbox"==c.type||"radio"==c.type?d.utils.registerEventHandler(c,
"change",function(){j[b.metadataProperty].accessed(!0)}):(d.utils.registerEventHandler(c,"blur",function(){j[b.metadataProperty].accessed(!0)}),d.utils.registerEventHandler(c,"focusout",function(){j[b.metadataProperty].accessed(!0)}));var k={},n=f(c),q=function(b,a){if(!f.fn.qtip)return{destroy:function(){}};var c=b.data("qtip");b.removeData("qtip");var d=b.qtip(f.extend({},a)).qtip("api");b.data("qtip",c);return d},m={};d.utils.arrayForEach(["$form","$user","$opt"],function(a){m[a]=b.BindingHandlers.nearestVariable(a,
l)});if(!m.$form||!m.$user)throw Error("Unable to find $form or $user in binding context.");m.$opt||(m.$opt={});m.$form.isReadOnly?n.prop(n.is('select, option, button, input[type="checkbox"], input[type="radio"]')?"disabled":"readOnly",!0):m.$user.role.isReviewer&&(k.monitor=q(n,{position:{my:"middle left",at:"middle right"},content:{text:'<input type="checkbox" class="rcc-monitor-include" title="Inkludera">'},suppress:!1,events:{render:function(a,c){d.applyBindingsToNode(c.elements.tooltip.find(".rcc-monitor-include")[0],
{checked:j[b.metadataProperty].include},l)}},hide:{event:!1,inactive:3E3}}),d.computed(function(){d.utils.toggleDomNodeCssClass(c,a,j[b.metadataProperty].include())}));d.computed(function(){if(d.utils.unwrapObservable(m.$opt.displayNames)&&n.is(":visible")){if(!k.name)k.name=q(n,{position:{my:"middle left",at:"middle right"},content:{text:f("<div>").text(j[b.metadataProperty].regvarName).html()},suppress:false,show:{ready:true},hide:{event:false}})}else if(k.name){k.name.destroy();delete k.name}});
"tooltip"in j[b.metadataProperty]&&(j[b.metadataProperty].tooltip.hasOwnProperty("html")?k.tooltip=q(n,{position:{my:"top center",at:"bottom center"},content:{text:'<div class="rcc-tooltip"></div>'},events:{render:function(a,c){d.applyBindingsToNode(c.elements.tooltip.find(".rcc-tooltip")[0],{html:j[b.metadataProperty].tooltip.html},l)}},show:{delay:1E3},hide:{event:"mouseleave"}}):j[b.metadataProperty].tooltip.hasOwnProperty("text")&&n.attr("title",j[b.metadataProperty].tooltip.text));d.utils.domNodeDisposal.addDisposeCallback(c,
function(){for(var a in k)k.hasOwnProperty(a)&&k[a].destroy()})}};d.utils.arrayForEach(["text","value","checked","textInput"],function(a){d.bindingHandlers[b.bindingsPrefix+a]={init:function(h,g,f,l,j){l=g();if(!d.isObservable(l)||!l[b.metadataProperty])throw Error(b.bindingsPrefix+a+" requires an observable from RCC.ViewModel as argument.");var k={};d.utils.arrayForEach([a,c],function(c){if(c in f())throw Error("Cannot bind "+b.bindingsPrefix+a+" and "+c+" to the same element.");k[c]=g()});d.applyBindingsToNode(h,
k,j)}}});d.bindingHandlers[b.bindingsPrefix+"list"]={init:function(a,h,g,f,l){var j=h(),h=j[b.metadataProperty];if(!h||!h.term||!h.term.listValues)throw Error('The binding "'+b.bindingsPrefix+'list" requires a list observable from RCC.ViewModel as argument.');g=g();h=h.term.listValues.filter(function(a){var b=new Date(a.validFrom),c=new Date,d=new Date(a.validTo);return j()&&j().id===a.id||!a.validFrom||!a.validTo||b<=c&&d>=c});j[b.metadataProperty].validation&&j[b.metadataProperty].validation.add(d.computed(function(){var a=
j();if(a&&a.validFrom&&a.validTo){var b=new Date(a.validFrom),c=new Date,a=new Date(a.validTo),d=function(a){var b=function(a){return 10>a?"0"+a:a};return a.getFullYear()+"-"+b(a.getMonth())+"-"+b(a.getDate())};if(b>c)return{result:"failed",fatal:!1,message:"Listv\u00e4rdet b\u00f6rjar g\u00e4lla f\u00f6rst "+d(b)};if(a<c)return{result:"failed",fatal:!1,message:"Listv\u00e4rdet utgick "+d(a)}}}));h={value:j,options:h,optionsText:"text",optionsCaption:"\u2013 V\u00e4lj \u2013"};h[c]=j;for(var k in h)h.hasOwnProperty(k)&&
g.hasOwnProperty(k)&&(h[k]=g[k]);d.applyBindingsToNode(a,h,l)}}})(window.jQuery,window.ko,window.RCC);(function(f,d,b){var a={};b.ViewModel=function(b){b||(b={});b.incaForm||(b.incaForm=f.form);if(!("rootTable"in b)){var e=[],h={},g;for(g in b.incaForm.metadata)if(b.incaForm.metadata.hasOwnProperty(g)){e.push(g);for(var p=b.incaForm.metadata[g].subTables,l=0;l<p.length;l++)h[p[l]]=!0}e=d.utils.arrayFilter(e,function(b){return!h[b]});if(1==e.length)b.rootTable=e[0];else throw Error('Must supply "rootTable" option for RCC.ViewModel ('+(0==e.length?"no candidates found":"multiple candidates found: "+
e.join(", "))+").");}b.incaUser||(b.incaUser=f.user);b.events||(b.events={});b.registerVariableDocumentation||(b.registerVariableDocumentation=d.observableArray(),b.registerId&&(e=f.getBaseUrl()+"api/Registers/"+b.registerId+"/RegisterVariables/Documentation",$.get(e).done(function(a){typeof a=="string"&&(a=JSON.parse(a));b.registerVariableDocumentation(a)})));e=a.createViewModel(b.rootTable,b.incaForm.data,b.incaForm.metadata,b.incaForm.createDataRow.bind(b.incaForm),[],b);e.$events=b.events;e.$env=
b.incaForm.env;e.$form=b.incaForm;e.$user=b.incaUser;e.$opt=b.opt||{};b.validation&&(e.$validation=b.validation,b.validation.track(e));return e};b.ViewModel.fill=function(b){return a.fillViewModel(b.target,b.data)};b.ViewModel.prototype.fill=function(b){return a.fillViewModel(this,b)};(function(a){var e,h,g,f,l,j,k,n,q,m,r,v,s,t;e="$$";h="$vd";g=b.metadataProperty;f="add";l="remove";j="include";k="accessed";n="regvar";q="regvarName";m="term";r="tooltip";v="documentation";s="dependencies";t="dependenciesFulfilled";
var u=function(){function b(a,c,e,h,i){var f=c[h].value,i=i||h;"list"===e.dataType?f=d.utils.arrayFirst(e.listValues,function(a){return a.id===f})||void 0:"decimal"===e.dataType&&"number"===typeof f&&(f=f.toFixed(e.precision).replace(/\./,","));a[i]=d.observable(f);a[i].subscribe(function(a){c[h].value=a&&"list"===e.dataType?a.id:a});if(g in a[i])throw Error('The created ko.observable already has a property with name "'+g+'"');a[i][g]={};a[i][g][j]=d.observable(c[h].include);a[i][g][j].subscribe(function(a){c[h].include=
a});a[i][g][k]=d.observable(!1);a[i][g][s]=d.observableArray();a[i][g][t]=d.computed(function(){var b=a[i][g][s]();return b&&b.reduce(function(a,b){return a&&b()},!0)});a[i][g][t].subscribe(function(b){b||(a[i](null),a[i][g][k](!1))})}function a(c,e,h,f){var i,j;for(i in h.regvars)h.regvars.hasOwnProperty(i)&&(j=h.terms[h.regvars[i].term],b(c,e.regvars,j,i,void 0),c[i][g][n]=h.regvars[i],c[i][g][m]=j,c[i][g][q]=i,function(){var a=i;c[a][g][v]=d.computed(function(){return _.find(f.registerVariableDocumentation(),
function(b){return b.shortName==a})})}(),h.regvars[i].description&&f&&f.opt&&("html"==f.opt.tooltips?c[i][g][r]={html:d.observable(h.regvars[i].description)}:"text"==f.opt.tooltips&&(c[i][g][r]={text:h.regvars[i].description})),f&&f.validation&&f.validation.init(c[i],{source:{viewModel:c[i],name:i,type:"regvar"}}))}function c(a,d,e,f){var i,j;a[h]={};for(i in e.vdlists)e.vdlists.hasOwnProperty(i)&&(j=e.terms[e.vdlists[i].term],b(a[h],d.vdlists,j,i,i),a[h][i][g][m]=e.terms[e.vdlists[i].term],f&&f.validation&&
f.validation.init(a[h][i],{source:{name:i,type:"vdlist"}}))}function x(a,b,c,h,i,j,k){var o,m,n,q;for(m=0;m<c.subTables.length;m+=1){o=c.subTables[m];a[e][o]=d.observableArray();a[e][o][g]={};a[e][o][g][f]=w.createAdd(a,o,h,i,j,k);a[e][o][g][l]=w.createRemove(a,o);for(n=0;n<b.subTables[o].length;n+=1)q=u(o,b.subTables[o][n],h,i,j,k),a[e][o].push(q)}}var w={createAdd:function(a,b,d,c,h,f){return function(g){g=g||u(b,void 0,d,c,h,f);a.$__data.subTables[b].push(g.$__data);a[e][b].push(g);return g}},
createRemove:function(a,b){return function(c){c.$__data.id?c.$__data._destroy=!0:d.utils.arrayRemoveItem(a.$__data.subTables[b],c.$__data);a[e][b].remove(c)}}};return function(b,h,f,g,i,j){var k=f[b],l={},p=!1;if(void 0===k)throw Error('Metadata for table "'+b+'" not found');void 0===h&&(h=g(b),p=!0);l.$__data=h;l[e]={};a(l,h,k,j);c(l,h,k,j);x(l,h,k,f,g,i.concat(l),j);p&&j&&j.events&&"rowCreated"in j.events&&d.utils.arrayForEach([].concat(j.events.rowCreated[b]||[]),function(a){a.call(l,l,i.slice(0))});
j&&j.events&&"rowAdded"in j.events&&d.utils.arrayForEach([].concat(j.events.rowAdded[b]||[]),function(a){a.call(l,l,i.slice(0))});return l}}();a.createViewModel=u;a.fillViewModel=function(a,b){var c,e;for(c in b)if(b.hasOwnProperty(c)&&d.isWriteableObservable(a[c]))if(e=a[c],"list"===e[g].term.dataType)a[c](d.utils.arrayFirst(e[g].term.listValues,function(a){return a.id==b[c]})||void 0);else if("decimal"===e[g].term.dataType&&"number"===typeof b[c])a[c](b[c].toFixed(e[g].term.precision).replace(/\./,
","));else a[c](b[c])}})(a)})(window.inca,window.ko,window.RCC,window);(function(f,d){d.Utils={};d.Utils.createDummyForm=function(b){function a(b,c){h[b]={regvars:{},subTables:{},vdlists:{}};d[b]={regvars:{},vdlists:{},subTables:[],terms:{}};f.each(["regvars","vdlists"],function(a,k){f.each(c[k]||{},function(a,c){g++;h[b][k][a]={include:!0,value:null};d[b][k][a]={term:g};d[b].terms[g]=f.isArray(c)?{description:"term for "+a,dataType:"list",listValues:f.map(c,function(a){return"object"==typeof a?a:{id:a,value:a,text:a}})}:c.term?c.term:{description:"term for "+a,dataType:c}})});
f.each(c.subTables||{},function(c,f){d[b].subTables.push(c);h[b].subTables[c]=[];a(c,f)})}var c={env:{_PERSNR:"19111111-1111",_FIRSTNAME:"Test",_SURNAME:"Testsson",_INREPNAME:"Rapport\u00f6r Rapport\u00f6rsson",_INUNITNAME:"OC Demo (0)",_SEX:"M",_ADDRESS:"Testgatan 1",_POSTALCODE:"12345",_CITY:"Teststad",_LKF:"123456",_SECRET:!1,_DATEOFDEATH:"",_REPORTERNAME:"",_LAN:"12",_KOMMUN:"34",_FORSAMLING:"56",_PATIENTID:1}},d={},h={},g=Math.round(1E3*Math.random());a(b.rootTable,{regvars:b.regvars,subTables:b.subTables});
c.metadata=d;c.createDataRow=function(a){return f.extend(!0,{},h[a])};c.data=c.createDataRow(b.rootTable);c.getContainer=function(){return document.getElementsByTagName("body")[0]};return c};d.Utils.parseYMD=function(b){return(b=(b||"").match(/^(\d{4})(-?)(\d\d)\2(\d\d)$/))?new Date(b[1],parseInt(b[3].replace(/^0/,""),10)-1,b[4].replace(/^0/,"")):void 0};d.Utils.ISO8601=function(b){function a(a){return("0"+a).slice(-2)}return[b.getFullYear(),a(b.getMonth()+1),a(b.getDate())].join("-")}})(window.jQuery,
window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation=function(b){b||(b={});this.viewModels=f.observableArray([]);this.errors=f.computed(d.Validation.prototype._getErrors.bind(this));this.requiredDefault="requiredDefault"in b?b.requiredDefault:!0;this.includedDefault="includedDefault"in b?b.includedDefault:!0;"addDefaultValidations"in b||(b.addDefaultValidations=!0);this.addDefaultValidations=b.addDefaultValidations;this._dummyModel={dummy:{}};this._dummyModel.dummy[d.metadataProperty]={term:{dataType:"rcc-dummy"}};this._dummyModel.dummy[d.metadataProperty].validation=
this.init(this._dummyModel.dummy,{source:{type:"unbound-errors"}});this.track(this._dummyModel)};d.Validation.prototype.add=function(){this._dummyModel.dummy[d.metadataProperty].validation.add.apply(this,Array.prototype.slice.call(arguments,0))};d.Validation.prototype.track=function(b){this.viewModels.push(b)};d.Validation.prototype.init=function(b,a){var c=this,e={};e.errors=f.observableArray([]);e.tests=f.observableArray([]);e.info=a;e.add=function(b){b=new d.Validation.Test({errorsTo:e.errors,
test:b,info:a});e.tests.push(b);return b};var h=b[d.metadataProperty];if(h&&(h.validation=e,c.addDefaultValidations)){var g=h.term;"datetime"==g.dataType&&!g.includeTime?e.add(new d.Validation.Tests.Date(b,{ignoreMissingValues:!0,fatal:!0})):"integer"==g.dataType?e.add(new d.Validation.Tests.Integer(b,{min:g.min,max:g.max,ignoreMissingValues:!0,fatal:!0})):"decimal"==g.dataType&&e.add(new d.Validation.Tests.Decimal(b,{decimalPlaces:g.precision,min:g.min,max:g.max,ignoreMissingValues:!0,fatal:!0}));
void 0!==g.maxLength&&null!==g.maxLength&&e.add(new d.Validation.Tests.Length(b,{max:g.maxLength,treatMissingLengthAsZero:!0,ignoreMissingValues:!0,fatal:!0}));void 0!==g.validCharacters&&null!==g.validCharacters&&e.add(new d.Validation.Tests.ValidCharacters(b,{characters:g.validCharacters,ignoreMissingValues:!0,fatal:!0}));f.utils.arrayForEach(["required","included"],function(a){var b=f.observable(void 0),d=f.observable(!1);e[a]=f.computed({read:function(){return d()?b():f.utils.unwrapObservable(c[a+
"Default"])},write:function(a){b(a);d(!0)}})});0>f.utils.arrayIndexOf(["boolean","rcc-dummy"],h.term.dataType)&&e.add(new d.Validation.Tests.NotMissing(b))}return e};d.Validation.prototype._forEachVariable=function(b,a){f.utils.arrayForEach(b,function e(b){for(var d in b)"$"!=d.substring(0,1)&&b.hasOwnProperty(d)&&b[d]&&a(b[d]);if(b.$vd)for(var p in b.$vd)b.$vd.hasOwnProperty(p)&&b.$vd[p]&&a(b.$vd[p]);if(b.$$)for(var l in b.$$)b.$$.hasOwnProperty(l)&&b.$$[l]&&f.utils.arrayForEach(b.$$[l](),e)})};
d.Validation.prototype._getErrors=function(){var b=[];this._forEachVariable(this.viewModels(),function(a){(a=a[d.metadataProperty])&&a.validation&&a.validation.errors&&f.utils.unwrapObservable(a.validation.included)&&b.push(a.validation.errors())});return f.utils.arrayGetDistinctValues(Array.prototype.concat.apply([],b))};d.Validation.prototype.markAllAsAccessed=function(){this._forEachVariable(this.viewModels(),function(b){(b=b[d.metadataProperty])&&b.accessed&&b.accessed(!0)})};d.Validation.prototype.markAllAsAccessedIfDependenciesAreFulfilled=
function(){this._forEachVariable(this.viewModels(),function(b){(b=b[d.metadataProperty])&&b.accessed&&b.dependenciesFulfilled()&&b.accessed(!0)})}})(window.ko,window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation.Test=function(b){var a=this;a._disposed=f.observable(!1);a._error={info:b.info};a._testReferences=[];a._targets=f.utils.arrayMap([].concat(b.errorsTo),function(b){var e=b[d.metadataProperty];return e&&e.validation?(a._testReferences.push(e.validation.tests),e.validation.tests.push(a),e.validation.errors):b});a._test=f.isSubscribable(b.test)?b.test:f.computed({read:b.test,disposeWhen:a._disposed});f.computed({read:a._update.bind(a),disposeWhen:a._disposed})};d.Validation.Test.prototype.dispose=
function(){var b=this;b._disposed(!0);f.utils.arrayForEach(b._testReferences,function(a){a.remove(b)});b._removeErrorFromTargets()};d.Validation.Test.prototype._removeErrorFromTargets=function(){var b=this;f.utils.arrayForEach(b._targets,function(a){a.remove(b._error)});delete b._error.message};d.Validation.Test.prototype._update=function(){var b=this;if(b._disposed())b._removeErrorFromTargets();else{var a=b._test();if("string"==typeof a)a={result:"failed",message:a};else if("undefined"==typeof a)a=
{result:"ok"};else if("object"!=typeof a)throw Error("Validation test returned bad value: "+a);if("failed"==a.result)b._error.message=a.message,b._error.fatal=a.fatal,b._error.data=a.data,f.utils.arrayForEach(b._targets,function(a){a.remove(b._error);a.push(b._error)});else if("ok"==a.result)b._removeErrorFromTargets();else throw Error("Validation test returned bad result: "+a.result);}}})(window.ko,window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation.Tests={};d.Validation.Tests._hasValue=function(b,a){return void 0!==b&&null!==b&&(!a||"string"!=typeof b||0<b.length)};d.Validation.Tests.Date=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};var e=d.Utils.parseYMD(c);return!e?{result:"failed",message:"Datumet kunde inte tolkas (\u00e4r inte p\u00e5 formen \u00c5\u00c5\u00c5\u00c5-MM-DD).",fatal:a.fatal}:
d.Utils.ISO8601(e)!=c?{result:"failed",message:"Felaktigt datum (inte p\u00e5 formen \u00c5\u00c5\u00c5\u00c5-MM-DD).",fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Integer=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(!d.Validation.Tests._hasValue(c)||!c.toString().match(/^[-+]?(?:\d|[1-9]\d*)$/))return{result:"failed",message:"Ogiltigt heltal.",fatal:a.fatal};
c=parseInt(c,10);return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna v\u00e4rde: "+a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna v\u00e4rde: "+a.max,fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Decimal=function(b,a){a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};var e=RegExp("^[-+]?(?:\\d|[1-9]\\d*),\\d{"+
a.decimalPlaces+"}$");if(!d.Validation.Tests._hasValue(c)||!c.toString().match(e))return{result:"failed",message:"Ej decimaltal med "+a.decimalPlaces+" decimal"+(a.decimalPlaces==1?"":"er")+".",fatal:a.fatal};c=parseFloat(c.toString().replace(/,/,"."));return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna v\u00e4rde: "+a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna v\u00e4rde: "+a.max,
fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Length=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(!d.Validation.Tests._hasValue(c)||typeof c.length=="undefined")if(a.treatMissingLengthAsZero)c=0;else return{result:"failed",message:"L\u00e4ngd saknas.",fatal:a.fatal};else c=c.length;return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna l\u00e4ngd: "+
a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna l\u00e4ngd: "+a.max,fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.ValidCharacters=function(b,a){a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(typeof c!="string")return{result:"failed",message:"Inte en textstr\u00e4ng.",fatal:a.fatal};for(var e=0;e<c.length;e++)if(a.characters.indexOf(c[e])<
0)return{result:"failed",message:"Otill\u00e5tet tecken: "+c[e],fatal:a.fatal};return{result:"ok"}}};d.Validation.Tests.NotMissing=function(b){return function(){var a=b[d.metadataProperty].validation.required();if(!a)return{result:"ok"};var c=b();if(!d.Validation.Tests._hasValue(c)||"string"==typeof c&&0==c.length){var e={result:"failed",message:"V\u00e4rde saknas"};"object"==typeof a&&null!==a&&f.utils.arrayForEach(["fatal","data"],function(b){a.hasOwnProperty(b)&&(e[b]=a[b])});return e}return{result:"ok"}}}})(window.ko,
window.RCC,window);

(function ( inca, window )
{
	var Utils = {};

	if ( !window.RCC )
		window.RCC = {};
	if ( !window.RCC.Vast )
		window.RCC.Vast = {};
	if ( !window.RCC.Vast.Utils )
		window.RCC.Vast.Utils = Utils;

	/**
	 * Beräknar antalet "fyllda år" för en person, där någon född på skottdagen
	 * anses fylla år på skottdagen vid skottår och annars den 1 mars.
	 *
	 * @param {Date} dateOfBirth Födelsedatum, i lokala tidszonen.
	 * @param {Date} date Datum vid vilken åldern ska beräknas, i lokala tidszonen.
	 * @returns {Integer} Antal fyllda år.
	 */
	Utils.calculateAge = function ( dateOfBirth, date )
	{
		if ( date.getTime() < dateOfBirth.getTime() )
			return undefined;

		function y ( date ) { return date.getFullYear(); };
		function m ( date ) { return date.getMonth(); };
		function d ( date ) { return date.getDate(); };

		// Beräkna först åldern vid årsslut (31 december).
		var age = y(date) - y(dateOfBirth);

		// Dra bort 1 år om aktuellt års födelsedag inte passerats. (Avgör hur skottårspersoner hanteras.)
		if ( m(date) < m(dateOfBirth) || (m(date) == m(dateOfBirth) && d(date) < d(dateOfBirth)) )
			age -= 1;

		return age;
	};

	/**
	 * Raderar alla rader i en tabell.
	 * @param table Tabell vars rader ska raderas.
	 */
	Utils.removeAllRows = function ( table )
	{
		$.each( table().slice(0), function (i, o) { table.rcc.remove(o); } );
	};

	/**
	 * Radera eller lägg till rader i undertabeller baserat på en variabels värde.
	 * Samma undertabell får inte hanteras i flera villkor.
	 *
	 * @param row Raden vars undertabeller ska administreras.
	 * @param currentValue Värdet som villkoren ska jämföras med. En ko.subscribable unwrappas 
	 *				först, och ett objekt reduceras till värdet av egenskapen "value".
	 * @param {Object[]} conditions Villkor av typen { table, value }, där tabellen table töms
	 *				om (i) aktuella värdet är skilt (!==) från value, eller (ii) om value är en array
	 *				skilt (!==) från varje element i arrayn, eller (iii) om value är en funktion och
	 *				denna funktion returnerar sant när den anropas med currentValue som argument.
	 *				I annat fall tillgodoses att tabellen har minst en rad.
	 */
	Utils.subscribeHelper = function ( row, currentValue, conditions )
	{
		// Unwrap observables and objects.
		currentValue = ko.utils.unwrapObservable(currentValue);
		if ( typeof currentValue == 'object' && currentValue !== null )
			currentValue = currentValue.value;

		$.each( conditions,
			function ( unused, condition )
			{
				var table = row.$$[condition.table];
				var shouldDelete;
				if ( $.isFunction(condition.value) )
					shouldDelete = !condition.value(currentValue, row);
				else
					shouldDelete = ($.inArray( currentValue, [].concat(condition.value) ) < 0);

				if ( shouldDelete )
					RCC.Vast.Utils.removeAllRows(table);
				else if ( table().length == 0 )
					table.rcc.add();
			} );
	};
	
	/**
	* Lägg till en rad i given tabell om denna saknar rader.
	*
	* @param table Tabellen vars rader ska administreras (en ko.observableArray med egenskap "rcc").
	* @returns {undefined|Object} Raden som lades till, eller undefined om ingen rad lades till.
	*/
	Utils.addRowIfEmpty = function ( table ) {
		if (table().length == 0) {
			return table.rcc.add();
		}
	};


	/**
	 * Tar fram vilken tabell som är rottabell, utgående från given metadata.
	 *
 	 * @param {Object} [metadata=inca.form.metadata] Metadataobjektet.
	 * @returns {String|undefined} Rottabellens namn, eller undefined om en unik rottabell 
	 * 			inte kan bestämmas.
	 */
	Utils.rootTable = function ( metadata )
	{
		if ( !metadata )
			metadata = inca.form.metadata;

		var tables = [], isSubTable = {};
		for ( var table in metadata )
		{
			if ( metadata.hasOwnProperty(table) )
			{
				tables.push( table );

				var subTables = metadata[table].subTables;
				for ( var i = 0; i < subTables.length; i++ )
					isSubTable[ subTables[i] ] = true;
			}
		}

		var rootTables = ko.utils.arrayFilter( tables, function ( table ) { return !isSubTable[table]; } );

		return rootTables.length == 1 ? rootTables[0] : undefined;
	};

	Utils.SubTableConditionHandler = function(rowAddedEventHandler, conditions) {

		// Sortera in villkor i en tabellstruktur och få ett objekt som visar vilka tabeller som ska få ett nytt rowAdded-event.
		var n_cond = {}
		$.each(inca.form.metadata, function (tableName, tableObj) {
			$.each(tableObj.regvars, function (regvarName) {
				if (conditions[regvarName]) {
					if (!n_cond[tableName]) { n_cond[tableName] = []; }
					n_cond[tableName].push({ name: regvarName, conditions: $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName] });
				}
			});
		});

		// Skapa rowAdded-event.
		$.each(n_cond, function (tableName, tableConditions) {
			function subTableConditionHandlerEvent(row) {
				$.each(tableConditions, function (i, condition) {
					row[condition.name].subscribe(function () {
						Utils.subscribeHelper(row, row[condition.name], condition.conditions);
					});
				});
			}

			if (rowAddedEventHandler[tableName]) {
				rowAddedEventHandler[tableName] = [].concat(rowAddedEventHandler[tableName], subTableConditionHandlerEvent);
			} else {
				rowAddedEventHandler[tableName] = subTableConditionHandlerEvent;
			}
		});

		/**
		* Hämtar värdet på ett villkor.
		*
		* @param {String} regvarName Namn på egenskap.
		* @param {String} tableName Namn på tabellen där raderna hanteras av subscribeHelper.
		* @returns {String|String[]} Värdet av villkoret.
		*/

		this.getConditionValue = function (regvarName, tableName) {
			var rval;
			var regvarConditions = $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName];
			$.each(regvarConditions, function (i, o) {
				if (o.table == tableName) {
					rval = o.value;
					return false;
				}
			});
			return rval;
		};

	};

	/**
	* Returnerar en array med samtliga value-egenskaper i angiven listvariabel.
	* @param {String} varName Namn på listvariabel.
	* @param {String|String[]} excludeValue Värde/värden som ska exkluderas.
	*/

	Utils.getListValuesArray = function(varName, excludeValue)
	{
		var listValues = (function() {
			var rv = null;
			$.each(inca.form.metadata, function(n, o) {
				if (rv) return false;
				$.each(o.regvars, function(nn, oo) {
					if (varName == nn)
					{
						rv = o.terms[oo.term].listValues;
						return false;
					}
				});
			});
			return rv;
		})();

		if (!listValues) throw new Error('List values for ' + varName + ' not found.');
		
		var rv = [];
		excludeValue = (excludeValue ? [].concat(excludeValue) : []);

		$.each(listValues, function(i, o) {
			if ($.inArray(o.value, excludeValue) == -1)
			{
				rv.push(o.value)
			}
		});

		return rv;
	};

	/**
	 * Markera given observable som "required" och "fatal". Felets dataobjekt innehåller
	 * objekt med egenskap "canBeSaved" satt till true.
	 *
	 * @param {ko.observable} Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 */

	Utils.markAsFatal = function(observable) {
		observable.rcc.validation.required({ fatal: true, data: { canBeSaved: true } });
	};

	/**
	 * Lägg till validering som kontrollerar att ett numeriskt värde ligger inom
	 * givna gränser, inklusivt. Valideringen görs endast om det verkligen är ett
	 * giltigt numeriskt värde.
	 * 
	 * @param {ko.observable} observable Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 * @param {Number} min Minsta tillåtna värde.
	 * @param {Number} max Största tillåtna värde.
	 * @param {Boolean} [isFatal=false] Om sant markeras felet som "fatal".
	 */

	Utils.addNumMinMaxValidation = function(observable, min, max, isFatal) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (typeof v == 'string')
				v = parseFloat(v.replace(/,/g, '.'));
			isFatal = !!isFatal;

			// Annan validering fångar felaktiga numeriska värden.
			if (v === undefined || v === null || isNaN(v) || !isFinite(v))
				return;
			else if (v > max)
				return { result: 'failed', message: 'Största tillåtna värde: ' + max.toString().replace('.', ','), fatal: isFatal };
			else if (v < min)
				return { result: 'failed', message: 'Minsta tillåtna värde: ' + min.toString().replace('.', ','), fatal: isFatal };
			else
				return;
		});
	};


	/**
	 * Lägg till validering, markerad som "fatal", som kontrollerar att en 
	 * observable innehåller ett värde som "rimligen" representerar ett årtal.
	 * @param {ko.observable} Observable från en RCC.ViewModel,  med .rcc.validation-egenskap.
	 */

	Utils.addYearValidation = function(observable) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (v !== null && v !== undefined && v !== '') {
				if (!(v + '').match(/^\d{4}$/)) {
					return { result: 'failed', message: 'Felaktigt årtal', fatal: true };
				}
			}
		});

	};

	/**
	 * Returnera en funktion som kan användas som inca.on('validation')-callback.
	 *
	 * @param {RCC.Validation} validation Valideringsobjektet.
	 * @param {Object} [actions={}] Åtgärder.
	 * @param {String[]} [actions.abort=[]] Åtgärder som inte kräver någon validering.
	 * @param {String[]} [actions.pause=[]] Åtgärder som inte kräver strikt validering.
	 * @returns {Function} Funktion som kan användas som inca.on('validation')-callback.
	 */
	Utils.getFormValidator = function ( validation, actions ) {
		if ( !actions ) actions = {};
		if ( !actions.pause ) actions.pause = [];
		if ( !actions.abort ) actions.abort = [];

		return function ( ) {
			var selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			var errors = validation.errors();

			if (errors.length > 0) {
				validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return true;
		};
	};

	/**
	 * Extraherar innehållet av den första kommentaren i funktionens kod.
	 *
	 * @param {Function} func Funktion som innehåller utkommenterad kod.
	 * @returns {String}
	 */
	Utils.extractSource = function(func) {
		var m = func.toString().match(/\/\*(?:\*\s*@(?:preserve|license))?([\s\S]*?)\*\//);
		return m && m[1];
	};
	
	/**
	 * Knockouts metod för att detektera version av Internet Explorer. Från knockout-3.2.0.debug.js
	 *
	 * @returns {Number|undefined}
	 */
    Utils.ieVersion = document && (function() {
        var version = 3, div = document.createElement('div'), iElems = div.getElementsByTagName('i');

        // Keep constructing conditional HTML blocks until we hit one that resolves to an empty fragment
        while (
            div.innerHTML = '<!--[if gt IE ' + (++version) + ']><i></i><![endif]-->',
            iElems[0]
        ) {}
        return version > 4 ? version : undefined;
    }());

})( window['inca'], window );

(function (RCC, ko, _) {
    ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var val = valueAccessor(),
                opt = _.defaults((ko.isObservable(val) ? { valueObservable: val } : val), { allowPointAsDecimalSeparator: false, padWithZeros: true }),
                metadata = opt.valueObservable[RCC.metadataProperty];

            if (!metadata || !metadata.term || metadata.term.dataType != "decimal")
                throw new Error('The binding "' + RCC.bindingsPrefix + 'decimal" requires a decimal observable from RCC.ViewModel as argument.');

            var precision = opt.valueObservable[RCC.metadataProperty].term.precision;
            var bindings = {
                event: {
                    change: function () {
                        var value = opt.valueObservable();
                        if (value) {
                            if (opt.allowPointAsDecimalSeparator) value = value.replace(/\./g, ',');
                            if (opt.padWithZeros && new RegExp("^-?(?:\\d+|\\d*,\\d{1," + precision + "})$").exec(value))
                                value = parseFloat(value.replace(/,/g, '.')).toFixed(precision).toString().replace(/\./g, ',');
                            opt.valueObservable(value);
                        }
                        return true;
                    }
                }
            };
            var valueBinding = {};
            valueBinding[RCC.bindingsPrefix + 'value'] = opt.valueObservable;
            ko.applyBindingsToNode(element, valueBinding, bindingContext);
            ko.applyBindingsToNode(element, bindings, bindingContext);
        }
    };
})(window['RCC'], window['ko'], window['_']);
(function ($, _, RCC, ko, moment) {
    ko.components.register('date-picker', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function(params) {
            if (!params.value) throw new Error('Parameter "value" is required.');
            _.defaults(params, { autoOpen: false, closable: true, readOnly: false });
           
            var self = this, today = (params.dateToday ? Moment(params.dateToday) : Moment());
            self.visible = ko.observable(params.autoOpen);
            self.pickerMonth = ko.observable();

            if (params.closable) {
                self.close = function() {
                    self.visible(false);
                };
                self.toggle = function() {
                    self.visible(!self.visible());
                };
            } else {
                self.toggle = function() {
                    self.visible(true);
                };
            }

            function Moment(args) {
                return moment(args).lang("sv");
            }

            function PickerMonth(date) {
                var observableDate = (function () {
                    var v = params.value(), m;
                    if (v) m = Moment(v);
                    if (m && m.isValid()) return m;
                })();

                var manualDate = (date ? Moment(date) : undefined);

                var baseDate = (function() {
                    if (manualDate) return manualDate;
                    else if (observableDate) return observableDate;
                    else return today;
                })();

                var pm = this;

                pm.title = (function(monthName, year) {
                    return monthName.substr(0, 1).toUpperCase() + monthName.substr(1) + ' ' + year;
                })(baseDate.format('MMMM'), baseDate.year());

                pm.prevMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).subtract('months', 1)));
                };

                pm.nextMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).add('months', 1)));
                };

                pm.weeks = (function (baseDateMonth, startAt) {
                    function PickerWeek(days) {
                        this.days = days;
                    }

                    function PickerDay(moment) {
                        this.date = moment.date();
                        this.isCurrentMonth = (baseDateMonth == moment.month());
                        this.isToday = today.isSame(moment, 'day');
                        this.isSelected = observableDate && observableDate.isSame(moment, 'day');
                       
                        this.click = function() {
                            
                            if (!params.readOnly) {
                                params.value(moment.format('YYYY-MM-DD'));
                                if (params.value.rcc.accessed) {
                                    params.value.rcc.accessed(true);
                                }
                            }
                            if (params.closable) self.visible(false);
                        };
                    }

                    startAt.subtract('days', startAt.isoWeekday() - 1);
                    return _.map(_.range(6), function(week) {
                        return new PickerWeek(_.map(_.range(7), function(day) {
                            return new PickerDay(moment(startAt).add('days', (week * 7) + day));
                        }));
                    });

                })(baseDate.month(), moment(baseDate).set('date', 1));
            }

            self.pickerMonth(new PickerMonth());
           
            params.value.subscribe(function (val) {
                if (val && val.match) {
                    var date;
                    if (val.match(/^\d{8}$/)) {
                        date = moment(val, 'YYYYMMDD');
                        if (date.isValid()) {
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    } else  if (val.match(/^\d{6}$/)) {
                        date = moment(val, 'YYMMDD');
                        if (date.isValid()) {
                            if (date.year() > moment().year()) {
                                date.subtract(100, 'years');
                            }
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    }
                    else  if (val.match(/\^d{2}-\d{2}-\d{2}$/)) {
                        date = moment(val, 'YY-MM-DD');
                        if (date.isValid()) {
                            if (date.year() > moment().year()) {
                                date.subtract(100, 'years');
                            }
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    }
                }
                self.pickerMonth(new PickerMonth());
            });
        },
        template: RCC.Vast.Utils.extractSource(function() {/**@preserve
            <div class="rcc-datepicker-wrapper">
                <button class="btn btn-xs btn-default rcc-datepicker-toggle" data-bind="click: toggle, css: { 'active': visible }">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
                <div class="rcc-datepicker" data-bind="with: pickerMonth, visible: visible">
                        <!-- ko if: $parent.close -->
                            <div class="div-close">
                                <button data-bind="click: $parent.close" class="close" aria-hidden="true">&times;</button>
                            </div>
                        <!-- /ko -->
                    <table class="table-condensed">
                        <thead>
                            <tr>
                                <th class="prev" data-bind="click: prevMonthClick">«</th>
                                <th class="month" colspan="5" data-bind="text: title"></th>
                                <th class="next" data-bind="click: nextMonthClick">»</th>
                            </tr>
                            <tr data-bind="foreach: ['Må', 'Ti', 'On', 'To', 'Fr', 'Lö', 'Sö']">
                                <th data-bind="text: $data"></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: weeks">
                            <tr data-bind="foreach: days">
                                <td data-bind="text: date, click: click, css: { 'diff-month': !isCurrentMonth, 'today': isToday, 'selected': isSelected }"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        */return undefined;})
    });

    ko.bindingHandlers[RCC.bindingsPrefix + 'datepicker'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            ko.applyBindingsToNode($('<date-picker></date-picker>').insertAfter(element)[0], { component: { name: 'date-picker', params: valueAccessor() } }, bindingContext);
            ko.applyBindingsToNode(element, { 'rcc-value': valueAccessor().value }, bindingContext);
        }
    };

})(jQuery, _, RCC, ko, moment);

(function(inca, RCC, ko, _) {
	ko.components.register('s-a', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.value && params.controlType != 'staticText') throw new Error('Parameter "value" is required.');
			var self = this;
			_.extend(self, _.omit(_.defaults(params, {
				question: params.value.rcc.regvar.label,
				info: '',
				controlType: { datetime: 'datepicker', list: 'select', boolean: 'checkbox' }[params.value.rcc && params.value.rcc.term.dataType] || 'text',
				fullWidthText: false,
				placeholder: '',
				enabled: true,
				showReviewerInclude: true,
				tooltip: params.value.rcc.regvar.description,
				dependenciesFulfilled: params.value.rcc.dependenciesFulfilled,
				optionsText: 'text',
                optionsCaption: '– Välj –',
                updateOnKeyPress: false
			}), 'controlType', 'controlChange'));

            self.helpPopover = ko.computed(function() {
                var helpText = params.help || (params.value.rcc && params.value.rcc.documentation && params.value.rcc.documentation() && params.value.rcc.documentation().helpText);
                return helpText ? {
                        title: params.value.rcc.regvar.label,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });

			if (self.showReviewerInclude)
				self.showReviewerInclude = !!(inca.user.role.isReviewer && !inca.form.isReadOnly && self.value.rcc && self.enabled);

		 	self.control = new function() {
				var m = this;
				m.type = (typeof params.controlType == 'string' ? params.controlType : params.controlType.name);
				m.options = _.defaults(params.controlType.options || {}, (function() {
					switch (m.type) {
						case 'datepicker': return { value: self.value, dateToday: inca.serverDate, readOnly: inca.form.isReadOnly || !self.enabled };
						case 'decimal': return { valueObservable: self.value };
						case 'select': return { value: self.value, caption: self.optionsCaption, text: self.optionsText };
						case 'staticText':
							if( _.isFunction(self.value) && _.isObject(self.value()) && self.value().text ){
								return { value: self.value().text };
							}
							return { value: self.value };
						default: return { value: self.value };
					}
				})());
                m.toggleInclude = function () {
                    self.value.rcc.include(!self.value.rcc.include());
                };
		 	};

			self.createControlEvent = function($parent) {
				return new function() {
					if (params.controlChange) this.change = function() { return params.controlChange($parent); };
				};
			};

			self.hasErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return errors && errors.length;
			});

			self.hasFatalErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
			});
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<!-- ko if: dependenciesFulfilled -->
				<div class="single-answer-template r" data-bind="attr: { title: tooltip }">
					<label data-bind="css: { 'accessed': value.rcc && value.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
						<span class="question" data-bind="popover: helpPopover, html: question + (helpPopover() ? ' <i class=\'fa fa-info-circle\'></i>' : '') + (info ? '<br /><span class=\'help\'>' + info + '</span>' : '')"></span>
						<!-- ko if: fullWidthText -->
							<br />
						<!--/ko-->
						<!-- ko if: ko.utils.unwrapObservable($root.showRegvarNames) && value.rcc -->
							<span class="regvar-name" data-bind="text: value.rcc.regvarName"></span>
						<!-- /ko -->
						<!-- ko with: _.extend(control, { event: createControlEvent($parent) }) -->
							<!-- ko if: type == 'select' -->
								<select class="ctrl" data-bind="enable: $parent.enabled, rcc-list: options.value, optionsCaption: options.caption, event: event, optionsText: options.text"></select>
							<!--/ko-->
							<!-- ko if: type == 'text' -->
								 <!-- ko if: $component.updateOnKeyPress -->
								 	<input class="ctrl" type="text" data-bind="rcc-textInput: options.value, event: event, enable: $parent.enabled" />
								 <!--/ko-->
								 <!-- ko ifnot: $component.updateOnKeyPress -->
								 	<input class="ctrl" type="text" data-bind="rcc-value: options.value, event: event, enable: $parent.enabled" />
								 <!--/ko-->
							<!--/ko-->
							<!-- ko if: type == 'datepicker' -->
	
								<input class="ctrl" type="text" data-bind="rcc-datepicker: options, event: event, enable: $parent.enabled" />
							<!--/ko-->
							<!-- ko if: type == 'staticText' -->
								<!-- ko text: options.value --><!-- /ko -->
							<!--/ko-->
							<!-- ko if: type == 'decimal' -->
								<input class="ctrl" type="text" data-bind="rcc-decimal: options, event: event, enable: $parent.enabled" />
							<!--/ko-->
							<!-- ko if: type == 'textarea' -->
								<textarea class="ctrl" type="text" data-bind="rcc-value: options.value, event: event, enable: $parent.enabled" rows="5" cols="50"></textarea>
							<!--/ko-->
							<!-- ko if: type == 'checkbox' -->
								<input type="checkbox" data-bind="rcc-checked: options.value, event: event, enable: $parent.enabled">
							<!--/ko-->
							
						<!--/ko-->
						<!-- ko if: placeholder -->
							<!-- ko text: placeholder --><!--/ko-->
						<!--/ko-->

						<!-- ko if: hasErrors -->
						<div class="error-list" data-bind="foreach: value.rcc.validation.errors" >
							<span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
								<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
								<span data-bind="text: message"></span>
							</span>
						</div>
						<!--/ko-->

					</label>
				</div>
			<!--/ko-->
		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);

(function(RCC, ko, _) {
	ko.components.register('m-a', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.table) throw new Error('Parameter "table" is required.');

			_.defaults(params, {
				question: '',
				info: '',
				minRows: 0,
				maxRows: Infinity,
				controls: [],
				showLegendIfEmpty: true
			});

			self.extendRoot = function($parent) {
				return new function() {
					var self = this;
					_.extend(self, _.pick(params, 'question', 'info', 'minRows', 'maxRows'));
					
					self.innerContent = (function() {
						var c = params.innerContent || [];
						return _.map(_.isArray(c) ? c : [c], function(x) {
							return _.defaults(x.object ? x : { object: x }, { type: 'component', margin: true });
						});
					})();

					self.tables = $parent.$$[params.table];
					self.showLegend = function() {
				 		if (self.tables().length > 0) return true;
						return (_.isBoolean(params.showLegendIfEmpty) ? params.showLegendIfEmpty : params.showLegendIfEmpty($parent));
					};

					self.controls = _.map(params.controls, function(control) {
						if (_.isString(control)) control = { name: control };
						var r = _.defaults(_.pick(control, 'question', 'info', 'placeholder', 'optionsText'), {
							question: '', info: '', placeholder: '', optionsText: 'text'
						});

						r.extendControl = function($parents) {
							return new function() {
								var x = this;
								x.value = $parents[0][control.name];
								x.type = control.type || { datetime: 'datepicker', list: 'select', boolean: 'checkbox' }[x.value.rcc.term.dataType] || 'text';
								x.options = _.defaults(control.options || {}, (function() {
									switch (control.type) {
										case 'datepicker': return { value: x.value, dateToday: inca.serverDate, readOnly: inca.form.isReadOnly };
										case 'decimal': return { valueObservable: x.value };
										case 'staticText':
											if( x.value() && typeof x.value() == 'object' && x.value().text ){
												return { value: x.value().text };
											}
											return { value: x.value };
										default: return { value: x.value };
									}
								})());

								if (_.isFunction(r.optionsText)) x.optionsText = function(listObject) {
									return control.optionsText(listObject, $parents);
								}
							};
						};
						return r;
					});
				}
			};
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div class="multi-answer-template" data-bind="with: _.extend($data, extendRoot($parent))">
				<!-- ko if: question && showLegend() -->
					<div class="r">
						<div class="b" data-bind="html: question"></div>
						<!-- ko if: info -->
							<span class="help" data-bind="html: info"></span>
						<!--/ko-->
					</div>
				<!--/ko-->
				<div data-bind="foreach: tables">
					<div data-bind="css: { 'multi-item': $parent.controls.length != 1 && $parent.maxRows != 1, 'single-item': $parent.controls.length == 1}">
						<div class="s-inl" data-bind="foreach: $parent.controls">
							<div class="s-inl" data-bind="with: _.extend($data, extendControl($parents))">
								<label data-bind="css: { 'accessed': value.rcc.accessed }">
									<select class="ctrl" data-bind="rcc-list: options.value, optionsText: optionsText"></select>
								</label>
							</div>
						</div>
						<!-- ko if: $parent.controls.length == 1-->
							<button class="btn btn-danger btn-xs"  data-bind="enable: !$root.$form.isReadOnly, click: $parent.tables.rcc.remove, visible: $parent.tables().length > $parent.minRows">Ta bort</button>
						<!-- /ko -->
						<!-- ko foreach: $parent.innerContent -->
							<!-- ko foreach: $parent.$$[object.params.table] -->
								<!-- ko foreach: $parent.object.params.controls -->
									<div style="margin-left: 25px">	
										<s-a params="value: $parent[name], question: question"></s-a>
									</div>
								<!--/ko-->
							<!--/ko-->
						<!--/ko-->
						<!-- ko if: $parent.controls.length != 1 && $parent.tables().length > $parent.minRows -->
							<div class="s"></div>
							<div class="r">
								<button class="btn btn-danger btn-xs" data-bind="enable: !$root.$form.isReadOnly, click: $parent.tables.rcc.remove">Ta bort</button>
							</div>
						<!--/ko-->
					</div>
				</div>
				<div class="r" data-bind="visible: $parent.tables().length < maxRows && showLegend()">
					<button class="btn btn-primary btn-xs" data-bind="enable: !$root.$form.isReadOnly, click: function() { $parent.tables.rcc.add(); }">Lägg till</button>
				</div>
			</div>
		*/return undefined;})
	});
})(window['RCC'], window['ko'], window['_']);

(function ($, RCC, inca) {
	var lists =
	{
		JaNej:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' }
		],

		JaNejIU:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		JaNejKanske:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Kanske', value: 'kanske' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		JaNejVejEj:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Vej ej', value: 'vet_ej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		JaNejOkant:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Okänt', value: 'okant' }
		],

		Sjukhus: [
			{ text: 'Arvika', value: 'arvika' },
			{ text: 'Borås', value: 'boras' },
			{ text: 'Danderyd', value: 'danderyd' },
			{ text: 'Eksjö', value: 'eksjo' },
			{ text: 'Eskilstuna', value: 'eskilstuna' },
			{ text: 'Falköping', value: 'falkoping' },
			{ text: 'Falun', value: 'falun' },
			{ text: 'Gällivare', value: 'gallivare' },
			{ text: 'Gävle', value: 'gavle' },
			{ text: 'Halmstad', value: 'halmstad' },
			{ text: 'Helsingborg', value: 'helsingborg' },
			{ text: 'Huddinge', value: 'huddinge' },
			{ text: 'Hudiksvall', value: 'hudiksvall' },
			{ text: 'Hässleholm', value: 'hassleholm' },
			{ text: 'Jönköping-Ryhov', value: 'jonkoping_ryhov' },
			{ text: 'Kalmar', value: 'kalmar' },
			{ text: 'Karlshamn', value: 'karlshamn' },
			{ text: 'Karlskoga', value: 'karlskoga' },
			{ text: 'Karlskrona', value: 'karlskrona' },
			{ text: 'Karlstad', value: 'karlstad' },
			{ text: 'Karolinska, Solna', value: 'karolinska_solna' },
			{ text: 'Katrineholm-Kullbergska', value: 'katrineholm_kullbergska' },
			{ text: 'Kungälv', value: 'kungalv' },
			{ text: 'Landskrona', value: 'landskrona' },
			{ text: 'Lindesberg', value: 'lindesberg' },
			{ text: 'Linköping', value: 'linkoping' },
			{ text: 'Ljungby', value: 'ljungby' },
			{ text: 'Lund', value: 'lund' },
			{ text: 'Löwenströmska', value: 'lowenstromska' },
			{ text: 'Malmö', value: 'malmo' },
			{ text: 'Malmö Psykiatriska Institut', value: 'malmo_psykiatriska_institut' },
			{ text: 'Mora', value: 'mora' },
			{ text: 'Motala', value: 'motala' },
			{ text: 'SU/Mölndal', value: 'su_molndal' },
			{ text: 'Norrköping', value: 'norrkoping' },
			{ text: 'Nyköping', value: 'nykoping' },
			{ text: 'Trollhättan NÄL', value: 'trollhattan_nal' },
			{ text: 'Piteå', value: 'pitea' },
			{ text: 'S:t Göran', value: 'st_goran' },
			{ text: 'SU/Östra sjukhuset', value: 'su_ostra_sjukhuset' },
			{ text: 'SU/Sahlgrenska', value: 'su_sahlgrenska' },
			{ text: 'Skellefteå', value: 'skelleftea' },
			{ text: 'Sollefteå', value: 'solleftea' },
			{ text: 'Sunderbyn', value: 'sunderbyn' },
			{ text: 'Sundsvall', value: 'sundsvall' },
			{ text: 'Säter', value: 'sater' },
			{ text: 'Södersjukhuset', value: 'sodersjukhuset' },
			{ text: 'Södertälje', value: 'sodertalje' },
			{ text: 'Trelleborg', value: 'trelleborg' },
			{ text: 'Uddevalla', value: 'uddevalla' },
			{ text: 'Umeå', value: 'umea' },
			{ text: 'Uppsala', value: 'uppsala' },
			{ text: 'Varberg', value: 'varberg' },
			{ text: 'Visby', value: 'visby' },
			{ text: 'Värnamo', value: 'varnamo' },
			{ text: 'Västervik', value: 'vastervik' },
			{ text: 'Västerås', value: 'vasteras' },
			{ text: 'Växjö', value: 'vaxjo' },
			{ text: 'WeMind Psykiatri, Haninge', value: 'haninge' },
			{ text: 'Ängelholm', value: 'angelholm' },
			{ text: 'Örebro', value: 'orebro' },
			{ text: 'Örnsköldsvik', value: 'ornskoldsvik' },
			{ text: 'Östersund', value: 'ostersund' },
			{ text: 'Capio Psykiatri Allmänpsykiatri Jakobsberg', value: 'jakobsberg'},
			{ text: 'Capio Psykiatri Ångest och depression', value: 'angest_depression'}
			{ text: 'Falkenberg', value: 'falkenberg'}
		],
		
		Indikation:
		[
			{ text: 'Lindrig depressiv episod F320', value: 'F320' },
			{ text: 'Medelsvår depressiv episod F321', value: 'F321' },
			{ text: 'Svår depressiv episod utan psykotiska symptom F322', value: 'F322' },
			{ text: 'Recidiverande depression, lindrig episod F330', value: 'F330' },
			{ text: 'Recidiverande depression, medelsvår episod F331', value: 'F331' },
			{ text: 'Recidiverande depression, svår episod utan psykotiska symptom F332', value: 'F332' },
			{ text: 'Bipolär sjukdom, lindrig eller medelsvår depressiv episod F313', value: 'F313' },
			{ text: 'Bipolär sjukdom, svår depressiv episod utan psykotiska symptom F314', value: 'F314' },
			{ text: 'Postpartum depression F530', value: 'F530' },
			{ text: 'Organiskt förstämningssyndrom F063', value: 'F063' },
			{ text: 'Annan indikation, ICD-kod kan anges', value: 'annan' },
			{ text: 'Annan indikation fritext', value: 'fritext' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },

		],

		Sjukstatus:
		[
			{ text: 'Ej bedömt', value: 'ej_bedomt' },
			{ text: 'Normal, inte alls sjuk', value: 'normalt' },
			{ text: 'Gränsfall för psykisk sjukdom', value: 'gransfall' },
			{ text: 'Lindrigt sjuk', value: 'lindrigt' },
			{ text: 'Måttligt sjuk', value: 'mattligt' },
			{ text: 'Påtagligt sjuk', value: 'patagligt' },
			{ text: 'Allvarligt sjuk', value: 'allvarligt' },
			{ text: 'Bland de mest extremt sjuka patienterna', value: 'extremt' },
		],

		Suicidforsok:
		[
			{ text: 'Nej', value: 'nej' },
			{ text: '1-2 ggr', value: '1-2_ggr' },
			{ text: '3 ggr eller fler', value: 'ge_3_ggr' },
			{ text: 'Uppgift saknas, otillräcklig information', value: 'uppgift_saknas' }
		],

		JaNejMADRS:
		[
			{ text: 'Ja, svar på varje delfråga finns', value: 'ja' },
			{ text: 'Ja, endast totalpoäng kan anges', value: 'ja_totalpoang' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		JaNejMADRSS:
		[
			{ text: 'Ja, svar på varje delfråga finns', value: 'ja' },
			{ text: 'Ja, endast totalpoäng kan anges', value: 'ja_totalpoang' },
			{ text: 'Nej, patienten kunde pga sitt psykiska tillstånd ej genomföra självskattning', value: 'nej_psyk_tillstand' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		JaNejEQ5D:
		[
			{ text: 'Ja, svar på varje delfråga finns', value: 'ja' },
			{ text: 'Nej, patienten kunde pga sitt psykiska tillstånd ej genomföra självskattning', value: 'nej_psyk_tillstand' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		CPRS:
		[
			{ text: '0 - Jag upplever ingen minnesstörning', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 - Jag upplever tillfälliga minnesstörningar', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 - Jag upplever besvärande eller generande', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 -Jag upplever en total oförmåga att minnas överhuvudtaget', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		Rorlighet:
		[
			{ text: 'Jag går utan svårigheter', value: '1' },
			{ text: 'Jag kan gå men med viss svårighet', value: '2' },
			{ text: 'Jag är sängliggande', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Hygien:
		[
			{ text: 'Jag behöver ingen hjälp med min dagliga hygien, mat eller påklädning', value: '1' },
			{ text: 'Jag har vissa problem att tvätta eller klä mig själv', value: '2' },
			{ text: 'Jag kan inte tvätta eller klä mig själv ', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		HuduvudsakligaAktiviteter:
		[
			{ text: 'Jag klarar av mina huvudsakliga aktiviteter', value: '1' },
			{ text: 'Jag har vissa problem att klara av mina huvudsakliga aktiviteter', value: '2' },
			{ text: 'Jag klarar inte av mina huvudsakliga aktiviteter', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		SmartorBesvar:
		[
			{ text: 'Jag har varken smärtor eller besvär', value: '1' },
			{ text: 'Jag har måttliga smärtor eller besvär', value: '2' },
			{ text: 'Jag har svåra smärtor eller besvär', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		OroNedstamdhet:
		[
			{ text: 'Jag är inte orolig eller nedstämd', value: '1' },
			{ text: 'Jag är orolig eller nedstämd i viss utstäckning', value: '2' },
			{ text: 'Jag är i högsta grad orolig eller nedstämd', value: '3' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Vardform:
		[
			{ text: 'Öppen vård', value: 'oppen' },
			{ text: 'Sluten vård', value: 'sluten' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Spoltyp:
		[
			{ text: '8-spole', value: '8_spole' },
			{ text: 'Vinklad 8-spole', value: 'vink_8_spole' },
			{ text: 'H-spole', value: 'h_spole' },
			{ text: 'Annan (specificera i fritext)', value: 'annan' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Stimulator:
		[
			{ text: 'Magventure R30', value: '1' },
			{ text: 'Magventure X100', value: '2' },
			{ text: 'Magstim', value: '3' },
			{ text: 'Mag & More', value: '4' },
			{ text: 'Annan apparat', value: 'annan' }
		],
		ThetaburstProtokoll:
		[
			{ text: 'iTBS', value: 'itbs' },
			{ text: 'cTBS', value: 'ctbs' },
			{ text: 'Annan (specificera i fritext)', value: 'annan' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		/*Pulsmonster:
		[
			{ text: 'Bifasisk', value: 'bifasisk' },
			{ text: 'Theta-burst', value: 'theta_burst' },
			{ text: 'Annat (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],*/
		Lokalisation:
		[
			{ text: 'Vä DLPFC', value: 'va_dlpfc' },
			{ text: 'Hö DLPFC', value: 'ho_dlpfc' },
			{ text: 'DMPFC', value: 'dmpfc' },
			{ text: 'Vä TPJ', value: 'va_tpj' },
			{ text: 'Annat område (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		LokalisationMetod:
		[
			{ text: '5-(6)cm regeln', value: '5cm_regeln' },
			{ text: '10-20 systemet', value: '10_20_systemet' },
			{ text: 'Neuronavigation', value: 'neuronavigation' },
			{ text: 'Annat (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		Motortroskelmetod:
		[
			{ text: 'EMG', value: 'emg' },
			{ text: 'Okulär besiktning hand', value: 'okular_besiktning_hand' },
			{ text: 'Okulär besiktning fot', value: 'okular_besiktning_fot' },
			{ text: 'Annat område (specificera i fritext)', value: 'annat' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		BiverkanKomplikation:
		[
			{ text: 'Smärta under stimulering', value: 'smarta_stimulering' },
			{ text: 'Annan huvudvärk', value: 'huvudvärk' },
			{ text: 'Svimning', value: 'svimning' },
			{ text: 'Kramp', value: 'kramp' },
			{ text: 'Yrsel', value: 'yrsel' },
			{ text: 'Muskelryckningar', value: 'muskelryckningar' },
			{ text: 'Ångest', value: 'angest' },
			{ text: 'Annan biverkan/komplikation', value: 'annan' },
		],
		OrsakAvslut:
		[
			{ text: 'Remission', value: 'remission' },
			{ text: 'Remission bedöms ej möjlig trots viss effekt', value: 'remission_bed_ej' },
			{ text: 'Brist på för behandlingen nödvändiga resurser', value: 'brist_beh_resurs' },
			{ text: 'Utebliven effekt', value: 'utebliven_effekt' },
			{ text: 'Biverkning', value: 'biverkning' },
			{ text: 'Ej avslutad', value: 'ej_avslutad' },
			{ text: 'Annan, vilken?', value: 'annan' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' },
		],

		KliniskBedomning:
		[
			{ text: 'Ej bedömd', value: 'ej_bedomd' },
			{ text: 'Väldigt mycket förbättrad', value: 'vald_mkt_forbattrad' },
			{ text: 'Mycket förbättrad', value: 'mkt_forbattrad' },
			{ text: 'Minimalt förbättrad', value: 'min_forbattrad' },
			{ text: 'Oförändrad', value: 'oforandrad' },
			{ text: 'Minimalt försämrad', value: 'min_forsamrad' },
			{ text: 'Mycket försämrad', value: 'mkt_forsamrad' },
			{ text: 'Väldigt mycket försämrad', value: 'vald_mkt_forsamrad' },
		],
		InfoMetod:
		[
			{ text: 'Skriftligt och muntligt', value: 'skriftligt_muntligt' },
			{ text: 'Skriftligt', value: 'skriftligt' },
			{ text: 'Mutligt', value: 'muntligt' },
			{ text: 'Inte alls', value: 'inte_alls' },
			{ text: 'Vet ej', value: 'vet_ej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		BesvarLangd:
		[
			{ text: 'Mindre än en månad', value: 'manad_mindre' },
			{ text: 'Mer än en månad, men besväret har gått över', value: 'manad_mer' },
			{ text: 'Jag har fortfarande besväret', value: 'fort_besvar' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Behandlingar6man:
		[
			{ text: 'Antidepressiva läkemedel', value: 'antidep' },
			{ text: 'Litium', value: 'litium' },
			{ text: 'Kognitiv beteendeterapi', value: 'kogn_terapi' },
			{ text: 'Annan psykologisk behandling', value: 'annan_psyk_beh' },
			{ text: 'Ingen av dessa', value: 'ingen' },
		],

		VardasFrivilligt: [
			{ text: 'Frivillig vård', 'value': 'frivillig_vard' },
			{ text: 'LPT', 'value': 'lpt' },
			{ text: 'LRV', 'value': 'lrv' },
			{ text: 'Uppgift saknas', 'value': 'uppgift_saknas' }
		],


		EgenSinnesstämning: [
			{ text: '0 Jag kan känna mig glad eller ledsen, allt efter omständigheterna.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag känner mig nedstämd för det mesta, men ibland kan det kännas lättare.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag känner mig genomgående nedstämd och dyster. Jag kan inte glädja mig (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag är totalt nedstämd och olycklig att jag inte kan tänka mig värre.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgnaOroskänslor: [
			{ text: '0 Jag känner mig mestadels lugn.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Ibland har jag obehagliga känslor av inre oro.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag har ofta en känsla av inre oro som ibland kan bli mycket stark, och som (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag har fruktansvärda, långvariga eller outhärdliga ångestkänslor.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenSömn: [
			{ text: '0 Jag sover lugnt och bra och tillräckligt länge för mina behov. Jag har inga (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag har vissa sömnsvårigheter. Ibland har jag svårt att somna eller sover (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag sover minst två timmar mindre per natt än normalt. Jag vaknar ofta (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag sover mycket dåligt, inte mer än 2-3 timmar per natt.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenMatlust: [
			{ text: '0 Min aptit är som den brukar vara.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Min aptit är sämre än vanligt.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Min aptit har nästan helt försvunnit. Maten smakar inte och jag måste (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag vill inte ha någon mat. Om jag skall få någonting i mig, måste jag (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenKoncentrationsförmåga: [
			{ text: '0 Jag har inga koncentrationssvårigheter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag har tillfälligt svårt att hålla tankarna samlade på sådant som normalt (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag har påtagligt svårt att koncentrera mig på sådant som normalt inte (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag kan överhuvudtaget inte koncentrera mig på någonting.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenInitiativförmåga: [
			{ text: '0 Jag har inga svårigheter med att ta itu med nya uppgifter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 När jag skall ta itu med något, tar det emot på ett sätt som inte är (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Det krävs en stor ansträngning för mig att ens komma igång med enkla (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag kan inte förmå mig att ta itu med de enklaste vardagssysslor.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgetKänslomässigtEngagemang: [
			{ text: '0 Jag är intresserad av omvärlden och engagerar mig i den, och det bereder (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Jag känner mindre starkt för sådant som brukar engagera mig. Jag har (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag kan inte känna något intresse för omvärlden, inte ens för vänner (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag har slutat uppleva några känslor. Jag känner mig smärtsamt likgiltig (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenPessimism: [
			{ text: '0 Jag ser på framtiden med tillförsikt. Jag är på det hela taget ganska nöjd (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Ibland klandrar jag mig själv och tycker att jag är mindre värd än andra.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag grubblar ofta över mina misslyckanden och känner mig mindervärdig (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag ser allting i svart och kan inte se någon ljusning. Det känns som (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgenLivslust: [
			{ text: '0 Jag har normal aptit på livet.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Livet känns inte särskilt meningsfullt men jag önskar ändå inte att jag (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Jag tycker ofta det vore bättre att vara död, och trots att jag egentligen (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Jag är egentligen övertygad om att min enda utväg är att dö (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		EgetMinne: [
			{ text: '0 - Jag upplever ingen minnesstörning', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 - Jag upplever tillfälliga minnesstörningar', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 - Jag upplever besvärande eller generande minnesstörningar', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 - Jag upplever en total oförmåga att minnas överhuvud taget', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],



		Nedstämdhet: [
			{ text: '0 Neutralt stämningsläge. Kan känna såväl tillfällig munterhet som (...)', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Övervägande upplevelser av nedstämdhet men ljusare stunder förekommer.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Genomgående nedstämdhet och dyster till sinnes. Sinnesstämningen påverkas (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Genomgående upplevelser av maximal nedstämdhet.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }

		],

		SänktGrundstämning: [
			{ text: '0 Neutral stämningsläge.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Ser genomgående nedstämd ut, men kan tillfälligt växla till ljusare (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ser nedstämd och olycklig ut oavsett samtalsämne.', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Genomgående uttryck för extrem dysterhet, tungsinne eller förtvivlad olycka.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Ångestkänslor: [
			{ text: '0 Mestadels lugn.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Tillfälliga känslor av obehaglig psykisk spänning.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ständig känsla av inre oro, någon gång så intensiv att den endast med (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Långdragna ångestkänslor. Överväldigande känslor av skräck eller dödsångest (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		MinskadNattsömn: [
			{ text: '0 Sover som vanligt.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Måttliga insomningssvårigheter eller kortare, ytligare eller oroligare sömn (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Minskad sömntid (minst två timmar mindre än normalt). Vaknar ofta (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Mindre än två till tre timmars nattsömn totalt.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		MinskadAptit: [
			{ text: '0 Normalt eller ökad aptit.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Dålig matlust.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Aptit saknas nästan helt, maten smakar inte, måste tvinga sig att äta.', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Måste övertalas att äta något överhuvudtaget. Matvägran.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Koncentrationssvårigheter: [
			{ text: '0 Inga koncentrationssvårigheter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Tillfälligt svårt att hålla tankarna samlade vid t.ex. läsning eller TV-tittande.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Uppenbara koncentrationssvårigheter som försvårar läsning eller samtal.', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Kontinuerliga, invalidiserande koncentrationssvårigheter.', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		Initiativlöshet: [
			{ text: '0 Ingen svårighet att ta itu med nya uppgifter.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Lätta igångsättningssvårigheter.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Svårt att komma igång även med enkla rutinuppgifter, som kräver (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Oförmögen att ta initiativ till de enklaste aktiviteter. Kan inte påbörja (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		KänslomässigtEngagemang: [
			{ text: '0 Normalt intresse för omvärlden och för andra människor.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Svårigheter att finna nöje i sådant som vanligen väcker intresse. Minskad (...)', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ointresserad av omvärlden. Upplevelser av likgiltighet inför vänner (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Total oförmåga att känna adekvat sorg eller vrede. Totalt eller smärtsam (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		DepressivtTankeinnehåll: [
			{ text: '0 Inga pessimistiska tankar.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Fluktuerande självförebråelser och mindervärdesidéer.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Ständiga självanklagelser. Klara, men inte orimliga, tankar om synd (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Absurda föreställningar om ekonomisk ruin och oförlåtliga synder. Absurda (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],

		LivsledaSjälvmordstankar: [
			{ text: '0 Ordinär livslust. Inga självmordstankar.', value: '0' },
			{ text: '1', value: '1' },
			{ text: '2 Livsleda, men inga eller endast vaga dödsönskningar.', value: '2' },
			{ text: '3', value: '3' },
			{ text: '4 Självmordstankar förekommer och självmord betraktas som en tänkbar utväg (...)', value: '4' },
			{ text: '5', value: '5' },
			{ text: '6 Uttalande avsikter att begå självmord, när tillfälle bjuds. Aktiva (...)', value: '6' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
	};

	// Add dummy ids.
	var n = 0;
	$.each(lists,
		function (unused, list) {
			n++;
			$.each(list,
				function (i, opt) {
					if (!opt.hasOwnProperty('id'))
						opt.id = n * 10000 + i;
				});
		});

	var q = function (x, y) { return x; };

	$.extend(true, inca.form, RCC.Utils.createDummyForm(
	{
		rootTable: 'Behandling',
		
		regvars:
		{
			datifylld: q('datetime', 'NY'),
			enhet: q('integer', 'NY'),
			sjukhus: q(lists.Sjukhus, 'Ny'),
			r_indikation: q(lists.Indikation, 'NY'),
			foreCGI: q(lists.Sjukstatus, 'NY'),
			suicidforsok: q(lists.Suicidforsok, 'NY'),
			foreMADRS: q(lists.JaNejMADRS, 'NY'),
			foreMADRSS: q(lists.JaNejMADRSS, 'NY'),
			foreEQ5D: q(lists.JaNejEQ5D, 'NY'),
			foreMinnesstorning: q(lists.CPRS, 'NY'),
			vardform: q(lists.Vardform, 'NY'),
			vardasFrivilligt: q(lists.VardasFrivilligt, 'NY'),
			tidigareRTMS: q(lists.JaNejIU, 'NY'),
			tidigareECT: q(lists.JaNejIU, 'NY'),
			forstaBehTillfalle: q('datetime', 'NY'),
			sistaBehTillfalle: q('datetime', 'NY'),
			dummy: q('datetime', 'NY'),
			antalBehTillfalle: q('integer', 'NY'),
			spoltyp: q(lists.Spoltyp, 'NY'),
			stimulator: q(lists.Stimulator, 'NY'),
			//pulsmonster: q(lists.Pulsmonster, 'NY'),
			thetaburstProtokoll: q(lists.ThetaburstProtokoll, 'NY'),
			lokalisation: q(lists.Lokalisation, 'NY'),
			lokalisationMetod: q(lists.LokalisationMetod, 'NY'),
			motortroskelMetod: q(lists.Motortroskelmetod, 'NY'),
			motortroskel: q('integer', 'NY'),
			titrering: q(lists.JaNejIU, 'NY'),
			sistaStyrka: q('integer', 'NY'),
			dosFrekvens: q('integer', 'NY'),
			dosPuls: q('integer', 'NY'),
			dosTidPulstag: q('integer', 'NY'),
			dosAntalPulstag: q('integer', 'NY'),
			dosTotAntalPulser: q('integer', 'NY'),
			dosTotBehDuration: q('integer', 'NY'),
			dosTotBehDurationSekunder: q('integer', 'NY'),
			antiDep: q(lists.JaNejIU, 'NY'),
			litium: q(lists.JaNejIU, 'NY'),
			lamotrigin: q(lists.JaNejIU, 'NY'),
			valproat: q(lists.JaNejIU, 'NY'),
			bensodiazepin: q(lists.JaNejIU, 'NY'),
			antiEp: q(lists.JaNejIU, 'NY'),
			antiPsyk: q(lists.JaNejIU, 'NY'),
			planFortsattRTMS: q(lists.JaNejIU, 'NY'),
			planECT: q(lists.JaNejIU, 'NY'),
			planSysPsykBeh: q(lists.JaNejIU, 'NY'),
			biverkanKomplikation: q(lists.JaNejIU, 'NY'),
			orsakAvslut: q(lists.OrsakAvslut, 'NY'),
			kliniskBedomning: q(lists.KliniskBedomning, 'NY'),
			efterMinnesstorning: q(lists.CPRS, 'NY'),
			efterMADRS: q(lists.JaNejMADRS, 'NY'),
			efterMADRSS: q(lists.JaNejMADRSS, 'NY'),
			efterEQ5D: q(lists.JaNejEQ5D, 'NY'),
			rtmsIgen: q(lists.JaNejKanske, 'NY'),
			annanInfo: q(lists.JaNej, 'NY'),
			efterCGI: q(lists.Sjukstatus, 'NY'),
		},
		subTables:
		{
			Behandlingsnummer:
			{
				regvars:
				{
				   behandlingsnummer: q('integer', 'NY'),
				},
				vdlists: { },
				subTables: {} 
			},
			Uppfoljning:
			{
				regvars:
				{
					uppfDatifylld: q('datetime', 'NY'),
					patientenkatBesvarad: q(lists.JaNej, 'NY'),
					kvarstaendeBiverkan: q(lists.JaNejOkant, 'NY'),
				},
				vdlists: { rtms_behandlingar: 'vd' },
				subTables:
				{
					Patientenkat:
					{
						regvars:
						{
						   	patSvarDatRTMS: q('datetime', 'NY'),
						   	tillrackligInfo: q(lists.JaNejVejEj, 'NY'),
							infoMetod: q(lists.InfoMetod, 'NY'),
							hjalpteRTMS: q(lists.JaNejKanske, 'NY'),
							besvarRTMS: q(lists.JaNejVejEj, 'NY'),
							rtmsIgenUppf: q(lists.JaNejKanske, 'NY'),
							fragorKontaktEnhet: q(lists.JaNejIU, 'NY'),
							ovrigKommentar: q('string', 'NY'),
							uppfMinnesstorning: q(lists.CPRS, 'NY'),
							uppfMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
							uppfMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
							uppfMADRSSSomn: q(lists.EgenSömn, 'NY'),						
							uppfMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
							uppfMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
							uppfMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
							uppfMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
							uppfMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
							uppfMADRSSLivslust: q(lists.EgenLivslust, 'NY'),
							uppfMADRSSTotalpoang: q('integer', 'NY'),
							uppfEQ5DHalsotillstand: q('integer', 'NY'),
							uppfEQ5DRorlighet: q(lists.Rorlighet, 'NY'),
							uppfEQ5DHygien: q(lists.Hygien, 'NY'),
							uppfEQ5DHuvudsakAktiviteter: q(lists.HuduvudsakligaAktiviteter, 'NY'),
							uppfEQ5DSmartorBesvar: q(lists.SmartorBesvar, 'NY'),
							uppfEQ5DOroNedstamdhet: q(lists.OroNedstamdhet, 'NY')
						},
						vdlists: { },
						subTables: {
							Besvar:
							{
								regvars:
								{
								   besvarFritext: q('string', 'NY'),
								   besvarLangd: q(lists.BesvarLangd, 'NY'),
								},
								vdlists: { },
								subTables: {} 
							},

							Behandlingar6man:
							{
								regvars:
								{
								   behandlingar6man: q(lists.Behandlingar6man, 'NY')
								},
								vdlists: { },
								subTables: {} 
							},
						} 
					},

					KvarstaendeBiverkan:
					{
						regvars:
						{
							kvarstaendBiverkanFritext: q('string', 'NY')
						},
						vdlists: { },
						subTables: {} 
					}

				}
			},
			UnderCGI:
			{
				regvars:
				{
				   underCGI: q(lists.Sjukstatus, 'NY')
				},
				vdlists: { },
				subTables: {
					UnderCGISkattning:
					{
						regvars:
						{
						   underCGISkattning: q('datetime', 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},
			BehandlingsTillfalle:
			{
				regvars:
				{
				   behTillfalle: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			IndikationAnnan:
			{
				regvars:
				{
				   r_indikationAnnan: q('vd', 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			IndikationFritext:
			{
				regvars:
				{
				   r_indikationFritext: q('string', 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			Suicidforsok12man:
			{
				regvars:
				{
				   suicidforsok12man: q(lists.Suicidforsok, 'NY')
				},
				vdlists: { },
				subTables: {} 
			},

			ForeMADRSTotalpoang:
			{
				regvars:
				{
				   foreMADRSTotalpoang: q('integer', 'NY'),
				   foreMADRSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					ForeMADRSDelsvar:
					{
						regvars:
						{
							foreMADRSNedstamdhet: q(lists.Nedstämdhet, 'NY'),			
							foreMADRSSAnktGrundstamning: q(lists.SänktGrundstämning, 'NY'),							
							foreMADRSAngestkanslor: q(lists.Ångestkänslor, 'NY'),						
							foreMADRSMinskadNattsomn: q(lists.MinskadNattsömn, 'NY'),							
							foreMADRSMinskadAptit: q(lists.MinskadAptit, 'NY'),						
							foreMADRSKoncsvarigheter: q(lists.Koncentrationssvårigheter, 'NY'),							
							foreMADRSInitiativloshet: q(lists.Initiativlöshet, 'NY'),						
							foreMADRSKanslomassigtEng: q(lists.KänslomässigtEngagemang, 'NY'),					
							foreMADRSDepTankeinnehall: q(lists.DepressivtTankeinnehåll, 'NY'),							
							foreMADRSSjalvmordstankar: q(lists.LivsledaSjälvmordstankar, 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},
			
			ForeMADRSSTotalpoang:
			{
				regvars:
				{
				   foreMADRSSTotalpoang: q('integer', 'NY'),
				   foreMADRSSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					ForeMADRSSDelsvar:
					{
						regvars:
						{
							foreMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
							foreMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
							foreMADRSSSomn: q(lists.EgenSömn, 'NY'),						
							foreMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
							foreMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
							foreMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
							foreMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
							foreMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
							foreMADRSSLivslust: q(lists.EgenLivslust, 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},

			ForeEQ5D:
			{
				regvars:
				{
					foreEQ5DHalsotillstand: q('integer', 'NY'),
				    foreEQ5DDatum: q('datetime', 'NY'),
					foreEQ5DRorlighet: q(lists.Rorlighet, 'NY'),
					foreEQ5DHygien: q(lists.Hygien, 'NY'),
					foreEQ5DHuvudsakAktiviteter: q(lists.HuduvudsakligaAktiviteter, 'NY'),
					foreEQ5DSmartorBesvar: q(lists.SmartorBesvar, 'NY'),
					foreEQ5DOroNedstamdhet: q(lists.OroNedstamdhet, 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			UnderMADRSS:
			{
				regvars:
				{
				   underMADRSS: q(lists.JaNejMADRSS, 'NY'),
				},
				vdlists: { },
				subTables: {
					UnderMADRSSTotalpoang:
					{
						regvars:
						{
						   underMADRSSTotalpoang: q('integer', 'NY'),
						   underMADRSSDatum: q('datetime', 'NY')
						},
						vdlists: { },
						subTables: {
							UnderMADRSSDelsvar:
							{
								regvars:
								{
									underMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
									underMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
									underMADRSSSomn: q(lists.EgenSömn, 'NY'),						
									underMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
									underMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
									underMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
									underMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
									underMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
									underMADRSSLivslust: q(lists.EgenLivslust, 'NY'),
								},
								vdlists: { },
								subTables: {
									
								} 
							}
						} 
					},
					
				} 
			},
			
			SpoltypAnnat:
			{
				regvars:
				{
					spoltypAnnat: q('string', 'NY')
				}	,			
				vdlists: { },
				subTables: {} 
			},
			StimulatorAnnan:
			{
				regvars:
				{
					stimulatorAnnan: q('string', 'NY')
				}	,			
				vdlists: { },
				subTables: {} 
			},
			ThetaburstProtokollAnnan:
			{
				regvars:
				{
					thetaburstProtokollAnnan: q('string', 'NY'),
				},			
				vdlists: { },
				subTables: {} 
			},
			LokalisationAnnat:
			{
				regvars:
				{
					lokalisationAnnat: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {} 
			},
			LokalisationMetodAnnat:
			{
				regvars:
				{
					lokalisationMetodAnnat: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {} 
			},
			MotortroskelMetodAnnat:
			{
				regvars:
				{
					motortroskelMetodAnnat: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {} 
			},
			BiverkanKomplikation:
			{
				regvars:
				{
					biverkanKomplikationLista: q(lists.BiverkanKomplikation, 'NY'),
				},				
				vdlists: { },
				subTables: {
					BiverkanKomplikationAnnan:
					{
						regvars:
						{
							biverkanKomplikationAnnan: q('string', 'NY'),
						},				
						vdlists: { },
						subTables: {
							
						} 
					}
				} 
			},
			OrsakAvslutAnnan:
			{
				regvars:
				{
					orsakAvslutAnnan: q('string', 'NY'),
				},				
				vdlists: { },
				subTables: {
					
				} 
			},

			EfterMADRSTotalpoang:
			{
				regvars:
				{
				   efterMADRSTotalpoang: q('integer', 'NY'),
				   efterMADRSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					EfterMADRSDelsvar:
					{
						regvars:
						{
							efterMADRSNedstamdhet: q(lists.Nedstämdhet, 'NY'),			
							efterMADRSSAnktGrundstamning: q(lists.SänktGrundstämning, 'NY'),							
							efterMADRSAngestkanslor: q(lists.Ångestkänslor, 'NY'),						
							efterMADRSMinskadNattsomn: q(lists.MinskadNattsömn, 'NY'),							
							efterMADRSMinskadAptit: q(lists.MinskadAptit, 'NY'),						
							efterMADRSKoncsvarigheter: q(lists.Koncentrationssvårigheter, 'NY'),							
							efterMADRSInitiativloshet: q(lists.Initiativlöshet, 'NY'),						
							efterMADRSKanslomassigtEng: q(lists.KänslomässigtEngagemang, 'NY'),					
							efterMADRSDepTankeinnehall: q(lists.DepressivtTankeinnehåll, 'NY'),							
							efterMADRSSjalvmordstankar: q(lists.LivsledaSjälvmordstankar, 'NY')
						},
						vdlists: { },
						subTables: {} 
					},
				} 
			},
			
			EfterMADRSSTotalpoang:
			{
				regvars:
				{
				   efterMADRSSTotalpoang: q('integer', 'NY'),
				   efterMADRSSDatum: q('datetime', 'NY')
				},
				vdlists: { },
				subTables: {
					EfterMADRSSDelsvar:
					{
						regvars:
						{
							efterMADRSSinnesstamning: q(lists.EgenSinnesstämning, 'NY'),			
							efterMADRSSOroskanslor: q(lists.EgnaOroskänslor, 'NY'),							
							efterMADRSSSomn: q(lists.EgenSömn, 'NY'),						
							efterMADRSSMatlust: q(lists.EgenMatlust, 'NY'),							
							efterMADRSSKoncformaga: q(lists.EgenKoncentrationsförmåga, 'NY'),						
							efterMADRSSInitiativformaga: q(lists.EgenInitiativförmåga, 'NY'),													
							efterMADRSSKanslomassigtEng: q(lists.EgetKänslomässigtEngagemang, 'NY'),					
							efterMADRSSPessimism: q(lists.EgenPessimism, 'NY'),							
							efterMADRSSLivslust: q(lists.EgenLivslust, 'NY')
						},
						vdlists: { },
						subTables: {} 
					}
				} 
			},

			EfterEQ5D:
			{
				regvars:
				{
					efterEQ5DHalsotillstand: q('integer', 'NY'),
				    efterEQ5DDatum: q('datetime', 'NY'),
					efterEQ5DRorlighet: q(lists.Rorlighet, 'NY'),
					efterEQ5DHygien: q(lists.Hygien, 'NY'),
					efterEQ5DHuvudsakAktiviteter: q(lists.HuduvudsakligaAktiviteter, 'NY'),
					efterEQ5DSmartorBesvar: q(lists.SmartorBesvar, 'NY'),
					efterEQ5DOroNedstamdhet: q(lists.OroNedstamdhet, 'NY')
				},
				vdlists: { },
				subTables: {} 
			},
			AnnanInfo:
			{
				regvars:
				{
				   annanInfoFritext: q('string', 'NY'),
				},
				vdlists: { },
				subTables: {} 
			}	
		}
	}));

})(window['jQuery'], window['RCC'], window['inca']);

(function (inca, window) {

	var Utils = {};

	if (!window.RCC)
		window.RCC = {};
	if (!window.RCC.Vast)
		window.RCC.Vast = {};
	if (!window.RCC.Vast.Utils)
		window.RCC.Vast.Utils = {};

	window.RCC.Vast.Utils.Offline = Utils;

	window.queryString = (function () {
		var r = {};
		if (location.search) {
			var col = location.search.substr(1).split('&');
			for (var i = 0; i < col.length; i++) {
				var objName = col[i].split('=')[0];
				if (objName) r[objName] = col[i].substr(objName.length + 1);
			}
		}
		return r;
	})();

	window.showRegvarNames = (window.queryString['regvarNames'] == 'true');

	try
	{
		if (parent != window) parent.SandboxFrame = window;	
	}
	catch (ex) { }
	
	/**
	* Ändrar rottabellen genom att radera angivna tabeller med tillhörande undertabeller. Raderar även värdedomäner
	* som inte hör till den nya rottabellen.
	*
	* @param {String} rootTableName Namn på den nya rottabellen.
	* @param {String[]} tablesToDelete Namn på tabeller som ska raderas. 
	* @param {String[]} [tablesToKeep=[rootTableName]] Namn på tabeller som ska exkluderas.
	*/
	Utils.changeRootTable = function (rootTableName, tablesToDelete, tablesToKeep) {
		if (!tablesToKeep) {
			tablesToKeep = [rootTableName];
		} else if ($.inArray(rootTableName, tablesToKeep) == -1) {
			tablesToKeep.push(rootTableName);
		}

		var toDelete = [];
		function getTableNames(rootTableName) {
			$.each(inca.form.metadata[rootTableName].subTables, function (i, o) {
				if ($.inArray(o, tablesToKeep) == -1) {
					getTableNames(inca.form.metadata[rootTableName].subTables[i]);
				}
			});

			if ($.inArray(rootTableName, toDelete) == -1) {
				toDelete.push(rootTableName);
			}

		}

		$.each(tablesToDelete, function (i, o) { getTableNames(o); });
		$.each(toDelete, function (i, o) { delete inca.form.metadata[o]; });

		$.each(tablesToKeep, function (toKeepIndex, toKeep) {
			var st_del = [];
			$.each(inca.form.metadata[toKeep].subTables, function (subTableIndex, subTable) {
				if ($.inArray(subTable, toDelete) != -1) {
					st_del.push(subTableIndex);
				}
			});
			$.each(st_del, function (i, o) {
				inca.form.metadata[toKeep].subTables.splice(o, 1);
			});
		});

		// Delete all vdlists that do not belong to the root table.
		$.each(inca.form.metadata, function ( tableName, tableMetadata ) {
			if ( tableName !== rootTableName ) {
				tableMetadata.vdlists = {};
			}
		});

		inca.form.data = inca.form.createDataRow(rootTableName);
	};

})(window['inca'], window);

(function() {

	if (queryString.birthDate) {
		var birthDateInt = parseInt(queryString.birthDate);
		if (!isNaN(birthDateInt)) {
			inca.form.env._FODELSEDATUM = RCC.Utils.ISO8601(new Date(birthDateInt));
		}
	}

	switch (queryString.formType) {
		case 'uppfoljning': RCC.Vast.Utils.Offline.changeRootTable('Uppfoljning', ['Behandling']); break;
		case 'behandling': RCC.Vast.Utils.Offline.changeRootTable('Behandling', []); break;
		default: RCC.Vast.Utils.Offline.changeRootTable('Behandling', []); break;
	}

	inca.form.env._SEX = (queryString.gender == 'man' ? 'M' : 'F');

	$(function () {
		$("body").prepend($('<button class="btn btn-primary">Skicka</button>').click(function ( ) { SendForm(); }));
	});
	
})();
(function ($, _) {
	function findGetParam(val) {
		var result = "Not found";
		var tmp = [];
		location.search.substr(1).split("&").forEach(function (item) {
			tmp = item.split("=");
			if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
		});
		return result;
	}
	
	var reloadBtnHolder = null;
	function reloadSticky(){
	    if( reloadBtnHolder ){
	    	$(document.body).trigger("sticky_kit:recalc");
	    }
	}

	function sendIncaObjectToParent(){
		//Skicka till parent (chrome)
		setTimeout(function() {
			var incaData = window.inca;
			//decorate with usage
			var bodyHtml = $("body").html();
			_.map(incaData.form.metadata, function(t,tName){
				t.active = bodyHtml.indexOf(tName) >= 0;
				_.each(t.regvars, function(r,rName){
					r.active = bodyHtml.indexOf(rName) >= 0;
				});
			});
			window.parent.postMessage(JSON.stringify(incaData), "*");
			//window.parent.postMessage(JSON.stringify(vm), "*");
		}, 1000);
	}


	showRegvars = function(e){
		$(this).next(".regvars").toggle();
		reloadSticky();
		return false;
	}

	function showMetadata(metadata){
		var subTables = _.map(metadata, function(o){
			return o.subTables;
		});
		subTables = _.flatten(subTables);
		
		var root = _.difference(_.keys(metadata), subTables);
		if( root.length != 1 ){
			console.error((root.length==0?"Multiple roots":"No root") + " found: "+root);
			return;
		}
		root = root[0];

		var depth = 0;
		var debugMetadataBlock = $("#debugMetadata").find("div");
		function drawTable(tableName){
			var padding = 10*depth;
			var tableRow = $("<div style='padding-left: "+padding+"px;'>"+" <span>"+tableName+"</span></div>");

			var regvars = _.keys(metadata[tableName].regvars);
			if( regvars.length > 0 ){
				var tableSpan = tableRow.find("span");
				tableSpan.on('click', showRegvars);
				tableSpan.css("cursor", "pointer");
				tableSpan.css("color", "darkblue");
				
				var regvarHtml = "";
				_.each(regvars, function(rName){
					var regvar = metadata[tableName].regvars[rName];
					var color = "lightgreen";
					if( !regvar.active ) color = "salmon";
					var type = metadata[tableName].terms[regvar.term].dataType;
					regvarHtml += '<span style="color: '+color+';">'+(regvarHtml==""?'- ':'<br>- ')+rName+" ("+type+")</span>";
				});
				var hiddenRegvars = $("<div class='regvars' style='display: none;'>"+regvarHtml+"</div>");

				tableRow.append(" ("+regvars.length+")");

				tableRow.append(hiddenRegvars);
			}else{
				tableRow.css("color", "salmon");
				tableRow.append(" (0)");
			}


			debugMetadataBlock.append(tableRow);
			depth++;
			_.each(metadata[tableName].subTables, function(t){
				drawTable(t);
			});
			depth--;
		};

		debugMetadataBlock.html("");
		drawTable(root);
	}

	window.updateDebugData = function(inca){
		if( $("#debug").length ){
			if( !$("#debugMetadata").length ) $("#debug").append('<div id="debugMetadata"></div>');

			$("#debugMetadata").html('<h5>Metadata: <a id="mdShow" href="#">show</a> /	<a id="mdHide" href="#">hide</a></h5><div></div>');

			showMetadata(inca.form.metadata);

			$("#mdShow").on('click', function(e){
				e.preventDefault();
				var debugMetadataBlock = $("#debugMetadata").find("div.regvars").show();
				reloadSticky();
				return false;
			});
			$("#mdHide").on('click', function(e){
				e.preventDefault();
				var debugMetadataBlock = $("#debugMetadata").find("div.regvars").hide();
				reloadSticky();
				return false;
			});
		}
	};


	$(function(){
		if( !(document.domain == "localhost" || document.domain == "" || findGetParam("debug") == "true") || findGetParam("debug") == "false") return;

		var url = window.location.pathname;
		var filename = url.substring(url.lastIndexOf('/')+1);
		if( filename == "index.html" ){
			$("body div.l div.w").append('<div class="panel panel-warning"><div class="panel-heading">DEBUG (enbart för systemutvecklare)</div><div id="debug" class="panel-body"></div></div>');

			var debugBtnHolder = $('<div id="debugBtnHolder" class="clearfix"></div>');
			var fullViewBtn = $('<button class="btn btn-primary pull-left">Populate all tables</button>');
			fullViewBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("populateAllTables", "*");
				return false;
			});
			var reloadMetadataBtn = $('<button class="btn btn-warning pull-right">Reload metadata</button>');
			reloadMetadataBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("resendIncaObject", "*");
				return false;
			});
			var accessRegvarsBtn = $('<button class="btn btn-info pull-left">Access all regvars</button>');
			accessRegvarsBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("accessAllRegvars", "*");
				return false;
			});
			debugBtnHolder.append(fullViewBtn);
			debugBtnHolder.append(reloadMetadataBtn);
			debugBtnHolder.append(accessRegvarsBtn);
			$("#debug").append(debugBtnHolder);

			var reloadBtn = $('<button id="reloadBtn" class="btn btn-danger">RELOAD IFRAME</button>');
			reloadBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.src = iframe.src;
				return false;
			});
			reloadBtnHolder = $('<div id="reloadBtnHolder" style="float: left;"></div>');
			reloadBtnHolder.append(reloadBtn);
			$("body div.l div.w").append(reloadBtnHolder);
			reloadBtnHolder.stick_in_parent({parent: 'body', offset_top: 100});
			reloadSticky();
		}else if( filename == "formular.html" ){
			sendIncaObjectToParent();
		}
	});

	function listenMessage(msg) {
		if( msg.data == "populateAllTables" ){
			console.log("Populating all tables...");
			(function assertChildrenHaveRows(row){
				$.each( row.$$,
					function ( unused, table )
					{
						if ( table().length == 0 )
							table.rcc.add();

						$.each( table(), function ( unused, childRow ) { assertChildrenHaveRows(childRow); } );
					} );
			})(window.vm);
			sendIncaObjectToParent();
			return;
		}else if( msg.data == "resendIncaObject" ){
			sendIncaObjectToParent();
			return;
		}else if( msg.data == "accessAllRegvars" ){
			function parseStructure(subTables){
				_.each(subTables, function(st, stName){
					if( typeof st == "function" ){
						_.each(st(), function(table){
							parseSubtable(table);
						});
					}else{
						console.error("Subtable not a function: "+stName);
					}
				});
			}
			function parseSubtable(table, excludeSubtables){
				_.each(table, function(prop, propName){
					if( propName == "Nyreg" ){
						parseSubtable(prop, true);
					}

					if( typeof prop == "function" && prop.rcc && prop.rcc.accessed ){
						prop.rcc.accessed(true);
					}

					if( propName == "$$" && !excludeSubtables ){
						parseStructure(prop);
					}
				});
			}
			console.log("Marking all regvars as accessed...");
			parseStructure(window.vm.$$);
			return;
		}

		var inca = JSON.parse(msg.data);
		updateDebugData(inca);
		reloadSticky();
	}

	if (window.addEventListener) {
	    window.addEventListener("message", listenMessage, false);
	} else {
	    window.attachEvent("onmessage", listenMessage);
	}

})(window['jQuery'],window['_']);

function SjukhusModul(observable)
{
	var self = this;
	function sortAZ(arr)
	{
	    arr.sort(function (a, b) {
	        if (a.name < b.name) return -1;
	        if (a.name > b.name) return 1;
	        return 0;
	    })
	}

	self.regioner = (function() {
		var rv = [
		    {
		        name: 'Norra',
		        landsting: [
		            { name: 'Västerbottens läns landsting', sjukhus: ['umea', 'skelleftea'] },
		            { name: 'Landstinget Västernorrland', sjukhus: ['ornskoldsvik', 'sundsvall', 'solleftea'] },
		            { name: 'Norrbottens läns landsting', sjukhus: ['gallivare', 'pitea', 'sunderbyn'] },
		            { name: 'Region Jämtland Härjedalen', sjukhus: ['ostersund'] }
		        ]
		    },
		    {
		        name: 'Uppsala-Örebro',
		        landsting: [
		            { name: 'Region Örebro län', sjukhus: ['orebro', 'karlskoga', 'lindesberg'] },
		            { name: 'Landstinget i Uppsala län', sjukhus: ['uppsala'] },
		            { name: 'Landstinget Dalarna', sjukhus: ['sater', 'falun', 'mora'] },
		            { name: 'Landstinget Sörmland', sjukhus: ['nykoping', 'eskilstuna', 'katrineholm_kullbergska'] },
		            { name: 'Landstinget i Värmland', sjukhus: ['karlstad', 'arvika'] },
		            { name: 'Region Gävleborg', sjukhus: ['hudiksvall', 'gavle'] },
		            { name: 'Landstinget Västmanland', sjukhus: ['vasteras'] }
		        ]
		    },
		    {
		        name: 'Stockholm',
		        landsting: [
		            {
		                name: 'Stockholms läns landsting',
		                sjukhus: ['lowenstromska', 'danderyd', 'sodertalje', 'st_goran', 'haninge', 'huddinge', 'karolinska_solna', 'sodersjukhuset','jakobsberg', 'angest_depression']
		            },
		            { name: 'Region Gotland', sjukhus: ['visby'] }
		        ]
		    },
		    {
		        name: 'Sydöstra',
		        landsting: [
		            { name: 'Region Östergötland', sjukhus: ['linkoping', 'norrkoping', 'motala'] },
		            { name: 'Landstinget i Kalmar län', sjukhus: ['kalmar', 'vastervik'] },
		            { name: 'Region Jönköpings län', sjukhus: ['varnamo', 'eksjo', 'jonkoping_ryhov'] }
		        ]
		    },
		    {
		        name: 'Västra',
		        landsting: [
		            { name: 'Västra Götalandsregionen', sjukhus: ['boras', 'kungalv', 'su_sahlgrenska', 'su_ostra_sjukhuset', 'falkoping', 'su_molndal', 'trollhattan_nal', 'uddevalla'] },
		            { name: 'Region Halland, norra', sjukhus: ['varberg', 'falkenberg'] }

		        ]
		    },
		    {
		        name: 'Södra',
		        landsting: [
		            { name: 'Region Skåne', sjukhus: ['malmo_psykiatriska_institut', 'trelleborg', 'lund', 'malmo', 'kristianstad', 'hassleholm', 'helsingborg', 'angelholm', 'landskrona'] },
		            { name: 'Region Kronoberg', sjukhus: ['vaxjo', 'ljungby'] },
		            { name: 'Region Halland, södra', sjukhus: ['halmstad'] },
		            { name: 'Landstinget Blekinge', sjukhus: ['karlskrona', 'karlshamn'] }
		        ]
		    }
		];

		sortAZ(rv);
		return rv;

	})();
	
	self.visible = ko.observable(false);
	self.selectedRegion = ko.observable();
	self.selectedLandsting = ko.observable();
	self.selectedSjukhus = ko.observable();

	var allaSjukhus = observable.rcc.term.listValues;
	var allaLandsting = (function()
	{
		var rv = [];
		$.each(self.regioner, function (i, o) {
		    $.each(o.landsting, function (ii, oo) {
		        rv.push(oo);
		    });
		});

		sortAZ(rv);
		return rv;
	})();

	self.landsting = ko.computed(function ()
	{
	    var reg = self.selectedRegion();
	    if (reg)
	    {
	        return reg.landsting;
	    }
	    else
	    {
	        return allaLandsting;
	    }
	});

	self.sjukhus = ko.computed(function ()
	{
	    var ls = self.selectedLandsting();
	    if (ls)
	    {
	        return $.grep(allaSjukhus, function (x)
	        {
	            return $.inArray(x.value, ls.sjukhus) != -1;
	        });
	    }
	    else if (self.selectedRegion()) 
	    {
	        var sjukhusIRegion = [];
	        $.each(self.selectedRegion().landsting, function (i, o)
	        {
	            $.each(o.sjukhus, function (ii, oo)
	            {
	                sjukhusIRegion.push(oo);
	            }); 
	        });
	        return $.grep(allaSjukhus, function (x)
	        {
	            return $.inArray(x.value, sjukhusIRegion) != -1;
	        });
	    }
	    else
	    {
	        return allaSjukhus;
	    }
	});

	self.save = function()
	{
		var v = self.selectedSjukhus();
		observable(ko.utils.arrayFirst(allaSjukhus, function (x)
		{
			return x.value == v;
		}));
		self.visible(false);
	};

	observable.subscribe(function ()
	{
	    var v = observable();
	    self.selectedRegion(undefined);
	    self.selectedLandsting(undefined);
	    self.selectedSjukhus(v ? v.value : undefined);
	});
}
(function ( inca, ko, window )
{
/**
 * @namespace
 */
var ICD10 = {};

/**
 * Name of the value domains.
 * @private
 */
ICD10.valueDomainNames =
{
	code: 'VD_ICD10_Kod',
	chapters: 'VD_ICD10_Kapitel',
	chapterCodes: 'VD_ICD10_Kapitelkoder'
};

/**
 * Cached codes; id is key and value is the object that
 * ICD10.informationForId returns.
 * @private
 */
ICD10._cache = {};

/**
 * Return an object containing observables describing the code with the given id.
 * @param id {Number} The number.
 * @returns {Object} An object with properties (ko.observable:s)
 *		- text: human readable representation
 *		- error: true if an error occurred and false if not, undefined if pending.
 *		- pending: true if the request is pending, false otherwise.
 *		- [obj]: raw data object from value domain
 */
ICD10.informationForId =
function ( id )
{
	if ( !ICD10._cache.hasOwnProperty(id) )
	{
		var info =
		{
			text: ko.observable('Laddar...'),
			error: ko.observable(undefined),
			pending: ko.observable(true),
			obj: ko.observable(undefined)
		};

		ICD10._cache[id] = info;

		inca.form.getValueDomainValues(
			{
				vdlist: ICD10.valueDomainNames.code,
				parameters: { kod: id },
				success:
					function ( list )
					{
						info.pending(false);

						if ( list.length != 1 )
						{
							info.error(true);
							info.obj(undefined);
							info.text( 'Oväntat radantal (id ' + id + ', n = ' + list.length + ')' );
						}
						else
						{
							info.error(false);
							info.obj( list[0] );
							info.text( ICD10._formatCode(list[0]) );
						}
					},
				error:
					function ( err )
					{
						info.pending(false);
						info.error(true),
						info.obj(undefined);
						info.text( 'Oväntat fel (id ' + id + '): ' + err );
					}
			} );
	}
	
	return ICD10._cache[id];
};

/**
 * Return a human readable representation of a code object.
 *
 * @param row A single data row describing a code.
 * @returns {String} A human readable representation of the code.
 * @private
 */
ICD10._formatCode =
function ( row )
{
	return row.data.beskrivning + ' (' + row.data.kod + ')'
};

/**
 * Creates a view model suitable for rendering in template 'icd10-picker'.
 * @param opt {Object} Options.
 * @param opt.addCode Callback when a new code is added.
 * @param {String} [opt.defaultChapter] Default chapter to select.
 * @constructor
 */
ICD10.Picker =
function ( opt )
{
	var self = this;


	self.errors = ko.observableArray([]);
	self.addCode = opt.addCode;

	self.query = ko.observable('').extend( { throttle: 300 } );
	self.maxCodes = ko.observable(10);

	// [ { id: chapter-id, codes: observableArray-of-codes, data: column-data }, ... ]
	self.chapters = ko.observableArray();
	self.chapters(undefined);
	self._loadChapters( opt.defaultChapter );

	self.chapter = ko.observable(undefined);
	self.chapter.subscribe(
		function ( chapter )
		{
			if ( chapter && !chapter.codes() )
				self._loadCodesForSelectedChapter();

			self.query('');
			self.maxCodes(10);
		} );

	self.filteredCodes = ko.computed( self._filteredCodes, self );
};

/**
 * Return the codes containing the search query. The results are
 * sorted ascending by the code.
 * @private
 */ 
ICD10.Picker.prototype._filteredCodes =
function ( )
{
	var self = this;

	if ( !self.chapter() )
		return undefined;

	var codes = self.chapter().codes();
	if ( !codes )
		return undefined;

	var query = self.query().toLowerCase();
	return ko.utils.arrayFilter( codes,
		function ( code )
		{
			return (code.data.kod + code.data.beskrivning).toLowerCase().indexOf(query) >= 0;
		} ).sort(
			function ( a, b )
			{
				var aa = a.data.kod;
				var bb = b.data.kod;

				return aa > bb ? +1 : (aa < bb ? -1 : 0);
			} );
};

/**
 * Attempts to load a value domain containing the available chapters.
 *
 * @param {String} [defaultChapter] Default chapter to select after loading list of chapters.
 * @private
 */
ICD10.Picker.prototype._loadChapters =
function ( defaultChapter )
{
	var self = this;

	inca.form.getValueDomainValues(
		{
			vdlist: ICD10.valueDomainNames.chapters,
			success:
				function ( list )
				{
					var chapters = 
						ko.utils.arrayMap( list,
							function ( chapter )
							{
								chapter.codes = ko.observableArray();
								chapter.codes(undefined);
								return chapter;
							} );
					chapters.sort(
							function ( a, b )
							{
								var aa = a.data.forstakod;
								var bb = b.data.forstakod;
		
								return aa > bb ? +1 : (aa < bb ? -1 : 0);
							} );
					self.chapters( chapters );
					if ( defaultChapter )
						self.chapter( ko.utils.arrayFirst( chapters, function ( chapter ) { return chapter.data.kapitel == defaultChapter; } ) );
				},
			error: function ( err ) { self.errors.push( 'Lyckades inte ladda kapitlen: ' + JSON.stringify(err) ); }
		} );
};

/**
 * Attempts to load a value domain containing the codes for the currently selected chapter.
 * Retrieved codes are saved to the cache.
 *
 * @private
 */
ICD10.Picker.prototype._loadCodesForSelectedChapter =
function ( )
{
	var self = this;
	var chapterId = self.chapter().id;

	inca.form.getValueDomainValues(
		{
			vdlist: ICD10.valueDomainNames.chapterCodes,
			parameters: { kapitel: chapterId },
			success:
				function ( list )
				{
					self.chapter().codes( list );

					// Populate the cache.
					ko.utils.arrayForEach( list,
						function ( row )
						{
							if ( ICD10._cache.hasOwnProperty(row.id) )
								return;

							ICD10._cache[row.id] =
							{
								text: ko.observable( ICD10._formatCode(row) ),
								error: ko.observable(false),
								pending: ko.observable(false),
								obj: ko.observable(row)
							};
						} );
				},
			error: function ( err ) { self.errors.push( 'Lyckades inte ladda koder för kapitel med id ' + chapterId + ': ' + JSON.stringify(err) ); }
		} );
};

window.ICD10 = ICD10;

})( inca, ko, window );

	var eventHandlers = {},
	subTableConditions;

(function () {
  
	var markAsFatal = RCC.Vast.Utils.markAsFatal,
		addNumMinMaxValidation = RCC.Vast.Utils.addNumMinMaxValidation,
		addYearValidation = RCC.Vast.Utils.addYearValidation,
		addRowIfEmpty = RCC.Vast.Utils.addRowIfEmpty,
		removeAllRows = RCC.Vast.Utils.removeAllRows;
		
	function calculateTotalPoints(itemArr, totalPoang) {
		var allItems = _.every(itemArr, function(item){ return item(); });
		var missingItem = _.find(itemArr, function(obj) { return obj() && obj().value == 'uppgift_saknas'; });
		if(allItems && !missingItem) {
			var sum = 0;
			_.each(itemArr, function(obs) {
				sum = sum + parseInt(obs().value);
				totalPoang(sum);
			});
		}
		else totalPoang(null);
	}

	function customSubTableCondition(table, obs, value) {
		if(obs() && obs().value == value)
			addRowIfEmpty(table);
	}
	
	function customSubTableMultiCondition(table, obs, condition) {
		removeAllRows(table);
		if(obs() && ($.inArray(obs().value, condition) !== -1))
			addRowIfEmpty(table)
	};

	function isFutureDate(observable) {
		observable.rcc.validation.add(function() {
			if(observable()) {
				var vDate = RCC.Utils.parseYMD(observable());
				if(vDate && inca.serverDate.getTime() < vDate.getTime())
					return { result: 'failed', message: 'Datum efter dagens datum', fatal: true };
			}
		});
	};

	function foreRTMS(obs, condObs) {
		obs.rcc.validation.add(function() {
			if(!obs() || !condObs())
				return;
			var obsDate = RCC.Utils.parseYMD(obs());
			var obsCondDate = RCC.Utils.parseYMD(condObs());
			if(!obsDate || !obsCondDate)
				return;
			if(obsDate.getTime() > obsCondDate.getTime())
				return { result: 'failed', message: 'Skall vara före (eller samma som) första rTMS', fatal: false };
		});
	};

	function underRTMS(obs, obsForsta, obsSista) {
		obs.rcc.validation.add(function() {
			if(!obs() || !obsForsta() || !obsSista())
				return;
			var obsDate = RCC.Utils.parseYMD(obs());
			var obsForstaDate = RCC.Utils.parseYMD(obsForsta());
			var obsSistaDate = RCC.Utils.parseYMD(obsSista());
			if(!obsDate || !obsForstaDate || !obsSistaDate)
				return;
			if(obsDate.getTime() > obsForstaDate.getTime() && obsDate.getTime() < obsSistaDate.getTime())
				return;
			else
				return { result: 'failed', message: 'Skall vara mellan första och sista rTMS', fatal: false };
		});
	};

	function efterRTMS(obs, condObs) {
		obs.rcc.validation.add(function() {
			if(!obs() || !condObs())
				return;
			var obsDate = RCC.Utils.parseYMD(obs());
			var obsCondDate = RCC.Utils.parseYMD(condObs());
			if(!obsDate || !obsCondDate)
				return;
			if(obsDate.getTime() < obsCondDate.getTime())
				return { result: 'failed', message: 'Skall vara samma som eller efter sista rTMS', fatal: false };
		});
	};

	eventHandlers.rowAdded = {
		Behandling: function (r) {
			r.sjukhusModul = new SjukhusModul(r.sjukhus);
			r.enhet(inca.user.position.id);

			if(inca.errand) {
				r.sjukhus(ko.utils.arrayFirst(r.sjukhus.rcc.term.listValues, function (list) {
			        return list.text == inca.user.position.name || list.value == inca.user.position.name;
			    }));
			}
			r.sjukhusModul.selectedSjukhus(r.sjukhus() && r.sjukhus().value);
			
			r.thetaburstProtokoll.subscribe(function() {
				if(r.thetaburstProtokoll() && r.thetaburstProtokoll().value == 'itbs') {
					r.dosFrekvens(50);
					r.dosPuls(30);
					r.dosTidPulstag(8);
					r.dosAntalPulstag(20);
					r.dosTotAntalPulser(600);
					r.dosTotBehDuration(3);
					r.dosTotBehDurationSekunder(8);
				}	
			});

			r.calcMaxMinDate = function() {
				var m = _.map(r.$$.BehandlingsTillfalle(), function(obj) {
					return RCC.Utils.parseYMD(obj.behTillfalle()).getTime();
				});
				if(r.$$.BehandlingsTillfalle().length > 0) {
					r.forstaBehTillfalle(moment(_.min(m)).format('YYYY-MM-DD'));
					r.sistaBehTillfalle(moment(_.max(m)).format('YYYY-MM-DD'));
					r.antalBehTillfalle(m.length);
				}
				else {
					r.forstaBehTillfalle('');
					r.sistaBehTillfalle('');
					r.antalBehTillfalle('');
				}
			};

			r.dummy.extend({ notify: 'always' });
			r.dummy.subscribe(function() {
				var row = r.$$.BehandlingsTillfalle.rcc.add();
				row.behTillfalle(r.dummy());
				r.calcMaxMinDate();
			});

			r.forstaSistaRTMS = function() {
				if(!r.forstaBehTillfalle() || !r.sistaBehTillfalle())
					return;
				var forsta = RCC.Utils.parseYMD(r.forstaBehTillfalle());
				var sista = RCC.Utils.parseYMD(r.sistaBehTillfalle());
				if(!forsta || !sista)
					return;
				if(sista.getTime() < forsta.getTime())
					return { result: 'failed', message: 'Första rTMS skall vara före (eller samma som) sista rTMS', fatal: false };
			};

			r.forstaBehTillfalle.rcc.validation.add(r.forstaSistaRTMS);
			r.sistaBehTillfalle.rcc.validation.add(r.forstaSistaRTMS);

			addNumMinMaxValidation(r.antalBehTillfalle, 1, 999, false);
			addNumMinMaxValidation(r.motortroskel, 20, 90, false);
			addNumMinMaxValidation(r.sistaStyrka, 80, 150, false);
			addNumMinMaxValidation(r.dosTotBehDuration, 0, 200, false);
			addNumMinMaxValidation(r.dosTotBehDurationSekunder, 0, 59, false);

			isFutureDate(r.forstaBehTillfalle);
			isFutureDate(r.sistaBehTillfalle);
			isFutureDate(r.datifylld);
			isFutureDate(r.dummy);

			_.each(['sjukhus','datifylld','forstaBehTillfalle', 'sistaBehTillfalle','antalBehTillfalle'], function(name) { markAsFatal(r[name]); });
			
			r.foreMADRS.subscribe(function() {
				customSubTableMultiCondition(r.$$.ForeMADRSTotalpoang, r.foreMADRS, ['ja', 'ja_totalpoang']);
			});
			r.foreMADRSS.subscribe(function() {
				customSubTableMultiCondition(r.$$.ForeMADRSSTotalpoang, r.foreMADRSS, ['ja', 'ja_totalpoang']);
			});
			r.efterMADRS.subscribe(function() {
				customSubTableMultiCondition(r.$$.EfterMADRSTotalpoang, r.efterMADRS, ['ja', 'ja_totalpoang']);
			});
			r.efterMADRSS.subscribe(function() {
				customSubTableMultiCondition(r.$$.EfterMADRSSTotalpoang, r.efterMADRSS, ['ja', 'ja_totalpoang']);
			});


			/*r.dummy.rcc.validation.add(function(){
				if(!r.dummy() || r.$$.BehandlingsTillfalle().length == 0)
					return { result: 'failed', message: 'Värde saknas', fatal: true };
			});*/
		},
	
		Behandlingsnummer: function (r) {
			addNumMinMaxValidation(r.behandlingsnummer, 1, 999, false);
		},
		IndikationAnnan: function (r) {
			r.icd10Visible = ko.observable(false);
			r.icd10Text = ko.observable();
			r.icd10Picker = new ICD10.Picker({
				defaultChapter: 'Psykiska sjukdomar och syndrom samt beteendestörningar',
				addCode: function(code) {
					r.icd10Visible(false);
					r.r_indikationAnnan(code.id);
				}	
			});
			ko.computed(function() {
				var val = r.r_indikationAnnan();
				r.icd10Text(ICD10.informationForId(val).text());
			});
		},
		Uppfoljning: function(r) {
			if(inca.errand) {
				r.behandlingar = ko.observableArray(undefined);
				inca.form.getValueDomainValues({
					vdlist: 'VD_rtms_behandlingar',
					parameters: { patId: inca.form.env._PATIENTID },
					success: function (list) { r.behandlingar(list); }, 
					error: function (err) { console.log(err); } 
				});
			}
			markAsFatal(r.uppfDatifylld);
			isFutureDate(r.uppfDatifylld);
			if(inca.errand)
				markAsFatal(r.$vd.rtms_behandlingar);
		},
		Patientenkat: function (r) {
			ko.computed(function() {
				calculateTotalPoints([r.uppfMADRSSinnesstamning,r.uppfMADRSSOroskanslor,r.uppfMADRSSSomn,						
				r.uppfMADRSSMatlust,r.uppfMADRSSKoncformaga,r.uppfMADRSSInitiativformaga,r.uppfMADRSSKanslomassigtEng,					
				r.uppfMADRSSPessimism,r.uppfMADRSSLivslust], r.uppfMADRSSTotalpoang);
			});

			r.ovrigKommentar.rcc.validation.required(false);
		},
		ForeMADRSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.ForeMADRSDelsvar, p[0].foreMADRS, 'ja');
			foreRTMS(r.foreMADRSDatum, p[0].forstaBehTillfalle);
			isFutureDate(r.foreMADRSDatum);
		},
		ForeMADRSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([
					r.foreMADRSNedstamdhet,r.foreMADRSSAnktGrundstamning,r.foreMADRSAngestkanslor,r.foreMADRSMinskadNattsomn,							
					r.foreMADRSMinskadAptit,r.foreMADRSKoncsvarigheter,r.foreMADRSInitiativloshet,r.foreMADRSKanslomassigtEng,					
					r.foreMADRSDepTankeinnehall,r.foreMADRSSjalvmordstankar ], p[1].foreMADRSTotalpoang);
			});		
		},
		ForeMADRSSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.ForeMADRSSDelsvar, p[0].foreMADRSS, 'ja');
			foreRTMS(r.foreMADRSSDatum, p[0].forstaBehTillfalle);
			isFutureDate(r.foreMADRSSDatum);
		},
		ForeMADRSSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([r.foreMADRSSinnesstamning,			
					r.foreMADRSSOroskanslor,						
					r.foreMADRSSSomn,						
					r.foreMADRSSMatlust,							
					r.foreMADRSSKoncformaga,						
					r.foreMADRSSInitiativformaga,													
					r.foreMADRSSKanslomassigtEng,					
					r.foreMADRSSPessimism,							
					r.foreMADRSSLivslust ], p[1].foreMADRSSTotalpoang);
			});				
		},
		ForeEQ5D: function (r, p) {
			foreRTMS(r.foreEQ5DDatum, p[0].forstaBehTillfalle);
			isFutureDate(r.foreEQ5DDatum);
		},
		UnderMADRSS: function (r, p) {
			r.underMADRSS.subscribe(function() {
				customSubTableMultiCondition(r.$$.UnderMADRSSTotalpoang, r.underMADRSS, ['ja', 'ja_totalpoang']);
			});
		},
		UnderMADRSSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.UnderMADRSSDelsvar, p[1].underMADRSS, 'ja');
			underRTMS(r.underMADRSSDatum, p[0].forstaBehTillfalle, p[0].sistaBehTillfalle);
			isFutureDate(r.underMADRSSDatum);
		},
		UnderMADRSSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([r.underMADRSSinnesstamning,			
					r.underMADRSSOroskanslor,							
					r.underMADRSSSomn,						
					r.underMADRSSMatlust,							
					r.underMADRSSKoncformaga,						
					r.underMADRSSInitiativformaga,													
					r.underMADRSSKanslomassigtEng,					
					r.underMADRSSPessimism,							
					r.underMADRSSLivslust ], p[2].underMADRSSTotalpoang);
			});
		},
		UnderCGISkattning: function (r, p) {
			underRTMS(r.underCGISkattning, p[0].forstaBehTillfalle, p[0].sistaBehTillfalle);
			isFutureDate(r.underCGISkattning);
		},
		EfterMADRSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.EfterMADRSDelsvar, p[0].efterMADRS, 'ja');
			efterRTMS(r.efterMADRSDatum, p[0].sistaBehTillfalle);
			isFutureDate(r.efterMADRSDatum);
		},
		EfterMADRSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([
					r.efterMADRSNedstamdhet,r.efterMADRSSAnktGrundstamning,r.efterMADRSAngestkanslor,r.efterMADRSMinskadNattsomn,							
					r.efterMADRSMinskadAptit,r.efterMADRSKoncsvarigheter,r.efterMADRSInitiativloshet,r.efterMADRSKanslomassigtEng,					
					r.efterMADRSDepTankeinnehall,r.efterMADRSSjalvmordstankar ], p[1].efterMADRSTotalpoang);
			});		
		},
		EfterMADRSSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.EfterMADRSSDelsvar, p[0].efterMADRSS, 'ja');
			efterRTMS(r.efterMADRSSDatum, p[0].sistaBehTillfalle);
			isFutureDate(r.efterMADRSSDatum);
		},
		EfterMADRSSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([r.efterMADRSSinnesstamning,			
					r.efterMADRSSOroskanslor,						
					r.efterMADRSSSomn,						
					r.efterMADRSSMatlust,							
					r.efterMADRSSKoncformaga,						
					r.efterMADRSSInitiativformaga,													
					r.efterMADRSSKanslomassigtEng,					
					r.efterMADRSSPessimism,							
					r.efterMADRSSLivslust ], p[1].efterMADRSSTotalpoang);
			});				
		},
		EfterEQ5D: function (r, p) {
			efterRTMS(r.efterEQ5DDatum, p[0].sistaBehTillfalle);
			isFutureDate(r.efterEQ5DDatum);
		},
	};
})();

subTableConditions = new RCC.Vast.Utils.SubTableConditionHandler(
	eventHandlers.rowAdded,
	{
		r_indikation: [
			{ table: 'IndikationAnnan', value: 'annan' },
			{ table: 'IndikationFritext', value: 'fritext' },
		],
		suicidforsok: [
			{ table: 'Suicidforsok12man', value: ['1-2_ggr', 'ge_3_ggr'] }
		],
		foreEQ5D: [
			{ table: 'ForeEQ5D', value: 'ja' }
		],
		titrering: [
			{ table: 'Behandlingsnummer', value: 'ja' }
		],
		spoltyp: [
			{ table: 'SpoltypAnnat', value: 'annan' }
		],
		stimulator: [
			{ table: 'StimulatorAnnan', value: 'annan' }
		],
		thetaburstProtokoll: [
			{ table: 'ThetaburstProtokollAnnan', value: 'annan' }
		],
		lokalisation: [
			{ table: 'LokalisationAnnat', value: 'annat' }
		],
		lokalisationMetod: [
			{ table: 'LokalisationMetodAnnat', value: 'annat' }
		],
		motortroskelMetod: [
			{ table: 'MotortroskelMetodAnnat', value: 'annat' }
		],
		biverkanKomplikation: [
			{ table: 'BiverkanKomplikation', value: 'ja' }
		],
		biverkanKomplikationLista: [
			{ table: 'BiverkanKomplikationAnnan', value: 'annan' }
		],
		orsakAvslut: [
			{ table: 'OrsakAvslutAnnan', value: 'annan' }
		],
		efterEQ5D: [
			{ table: 'EfterEQ5D', value: 'ja' }
		],
		annanInfo: [
			{ table: 'AnnanInfo', value: 'ja' }
		],
		patientenkatBesvarad: [
			{ table: 'Patientenkat', value: 'ja' }
		],
		besvarRTMS: [
			{ table: 'Besvar', value: 'ja' }
		],
		kvarstaendeBiverkan: [
			{ table: 'KvarstaendeBiverkan', value: 'ja' }
		],
		underCGI: [
			{ table: 'UnderCGISkattning', value: function(value, row) {
				return value != 'ej_bedomt';
			}}
		]
	});

$(function () {

	var actions = { abort: ['Avbryt', 'Radera'], pause: ['Pausa'] };

	try {
		var vm = new RCC.ViewModel({
			validation: new RCC.Validation(),
			events: eventHandlers
		});

		// För offline.
		vm.showRegvarNames = ko.observable(window.showRegvarNames);

		var SendForm = function () {
			var errors = vm.$validation.errors(),
			selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			if (errors.length > 0) {
				vm.$validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);

				if(isPause) {
					return true;
				}

				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return true;
		};

		vm.pendingRequests = ko.observable(0);

		inca.on('validation', SendForm);
		// XXX: debug
		window.SendForm = SendForm;

		vm.errors = ko.observableArray([]);
		var ready = ko.observable(false);
		vm.showForm = ko.computed( function ( ) { return ready() && (vm.errors().length == 0 || vm.$form.isReadOnly) && vm.pendingRequests() == 0; } );

		vm.enableHelpText = ko.observable(true);
		vm.getSubTableConditionValue = subTableConditions.getConditionValue;
		vm.hasValue = function (observable, values) {
			var obsValue = observable();
			if (!obsValue) return false;
			if (typeof values == 'string') {
				values = [values];
			}
			return $.inArray(obsValue.value, values) != -1;
		};

		vm.isSavableRegisterRecord = !inca.errand && !inca.form.isReadOnly;
		

		if (!inca.errand) {
			vm.tabs = ko.observableArray([]);
			vm.activeTabIndex = ko.observable(0);

			vm.tabs.push({ template: 'behandling', data: vm, name: 'Behandling', tabdate: vm.datifylld });
			_.each(vm.$$.Uppfoljning(), function(row) {
				vm.tabs.push({ template: 'uppfoljning', data: row, name: 'Uppföljning', tabdate: row.uppfDatifylld });
			});
		}

		vm.removeRow = function (observableArray, row, name, event) {
			event.preventDefault();
			event.stopPropagation();

			if (confirm('Bekräfta borttag av "' + ko.unwrap(name) + '". Åtgärden går inte att ångra.')) {
				observableArray[RCC.metadataProperty].remove(row);

				if(vm.tabs) {
					_.each(vm.tabs(), function(tabRow) {
						if(row.uppfDatifylld() && row.uppfDatifylld() == tabRow.tabdate())
							vm.tabs.remove(tabRow);
					});	
				}
			
				if (vm.activeTabIndex() >= observableArray().length) {
					vm.activeTabIndex(vm.activeTabIndex() - 1);
				}	
			}
		};

		 _.each(['ICD10_Kapitel', 'ICD10_Kod', 'ICD10_Kapitelkoder'], function(s) {
		 	var vd = vm.$vd && vm.$vd[s];
		 	if (vd) vd.rcc.validation.required(false);
		});

		
		ko.applyBindings(vm);
		ready(true);

		// XXX: debug
		window.vm = vm;
	} catch (ex) {
		inca.on("validation", function () {
			if (inca.errand && inca.errand.action && $.inArray(inca.errand.action.find ? inca.errand.action.find(':selected').text() : undefined, actions.abort) != -1) return false;
		});
		alert("Ett fel inträffade\r\n\r\n" + ex + "\r\n" + JSON.stringify(ex));  
	}

});



