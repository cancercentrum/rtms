	var eventHandlers = {},
	subTableConditions;

(function () {
  
	var markAsFatal = RCC.Vast.Utils.markAsFatal,
		addNumMinMaxValidation = RCC.Vast.Utils.addNumMinMaxValidation,
		addYearValidation = RCC.Vast.Utils.addYearValidation,
		addRowIfEmpty = RCC.Vast.Utils.addRowIfEmpty,
		removeAllRows = RCC.Vast.Utils.removeAllRows;
		
	function calculateTotalPoints(itemArr, totalPoang) {
		var allItems = _.every(itemArr, function(item){ return item(); });
		var missingItem = _.find(itemArr, function(obj) { return obj() && obj().value == 'uppgift_saknas'; });
		if(allItems && !missingItem) {
			var sum = 0;
			_.each(itemArr, function(obs) {
				sum = sum + parseInt(obs().value);
				totalPoang(sum);
			});
		}
		else totalPoang(null);
	}

	function customSubTableCondition(table, obs, value) {
		if(obs() && obs().value == value)
			addRowIfEmpty(table);
	}
	
	function customSubTableMultiCondition(table, obs, condition) {
		removeAllRows(table);
		if(obs() && ($.inArray(obs().value, condition) !== -1))
			addRowIfEmpty(table)
	};

	function isFutureDate(observable) {
		observable.rcc.validation.add(function() {
			if(observable()) {
				var vDate = RCC.Utils.parseYMD(observable());
				if(vDate && inca.serverDate.getTime() < vDate.getTime())
					return { result: 'failed', message: 'Datum efter dagens datum', fatal: true };
			}
		});
	};

	function foreRTMS(obs, condObs) {
		obs.rcc.validation.add(function() {
			if(!obs() || !condObs())
				return;
			var obsDate = RCC.Utils.parseYMD(obs());
			var obsCondDate = RCC.Utils.parseYMD(condObs());
			if(!obsDate || !obsCondDate)
				return;
			if(obsDate.getTime() > obsCondDate.getTime())
				return { result: 'failed', message: 'Skall vara före (eller samma som) första rTMS', fatal: false };
		});
	};

	function underRTMS(obs, obsForsta, obsSista) {
		obs.rcc.validation.add(function() {
			if(!obs() || !obsForsta() || !obsSista())
				return;
			var obsDate = RCC.Utils.parseYMD(obs());
			var obsForstaDate = RCC.Utils.parseYMD(obsForsta());
			var obsSistaDate = RCC.Utils.parseYMD(obsSista());
			if(!obsDate || !obsForstaDate || !obsSistaDate)
				return;
			if(obsDate.getTime() > obsForstaDate.getTime() && obsDate.getTime() < obsSistaDate.getTime())
				return;
			else
				return { result: 'failed', message: 'Skall vara mellan första och sista rTMS', fatal: false };
		});
	};

	function efterRTMS(obs, condObs) {
		obs.rcc.validation.add(function() {
			if(!obs() || !condObs())
				return;
			var obsDate = RCC.Utils.parseYMD(obs());
			var obsCondDate = RCC.Utils.parseYMD(condObs());
			if(!obsDate || !obsCondDate)
				return;
			if(obsDate.getTime() < obsCondDate.getTime())
				return { result: 'failed', message: 'Skall vara samma som eller efter sista rTMS', fatal: false };
		});
	};

	eventHandlers.rowAdded = {
		Behandling: function (r) {
			r.sjukhusModul = new SjukhusModul(r.sjukhus);
			r.enhet(inca.user.position.id);

			if(inca.errand) {
				r.sjukhus(ko.utils.arrayFirst(r.sjukhus.rcc.term.listValues, function (list) {
			        return list.text == inca.user.position.name || list.value == inca.user.position.name;
			    }));
			}
			r.sjukhusModul.selectedSjukhus(r.sjukhus() && r.sjukhus().value);

			if(r.sjukhus() && r.sjukhus().value == 'falkoping') {
				r.sjukhus(ko.utils.arrayFirst(r.sjukhus.rcc.term.listValues, function (list) {
					return list.value == 'skovde';
				}));
			}
			
			r.thetaburstProtokoll.subscribe(function() {
				if(r.thetaburstProtokoll() && r.thetaburstProtokoll().value == 'itbs') {
					r.dosFrekvens(50);
					r.dosPuls(30);
					r.dosTidPulstag(8);
					r.dosAntalPulstag(20);
					r.dosTotAntalPulser(600);
					r.dosTotBehDuration(3);
					r.dosTotBehDurationSekunder(8);
				}	
			});

			r.calcMaxMinDate = function() {
				var m = _.map(r.$$.BehandlingsTillfalle(), function(obj) {
					return RCC.Utils.parseYMD(obj.behTillfalle()).getTime();
				});
				if(r.$$.BehandlingsTillfalle().length > 0) {
					r.forstaBehTillfalle(moment(_.min(m)).format('YYYY-MM-DD'));
					r.sistaBehTillfalle(moment(_.max(m)).format('YYYY-MM-DD'));
					r.antalBehTillfalle(m.length);
				}
				else {
					r.forstaBehTillfalle('');
					r.sistaBehTillfalle('');
					r.antalBehTillfalle('');
				}
			};

			r.dummy.extend({ notify: 'always' });
			r.dummy.subscribe(function() {
				var row = r.$$.BehandlingsTillfalle.rcc.add();
				row.behTillfalle(r.dummy());
				r.calcMaxMinDate();
			});

			r.forstaSistaRTMS = function() {
				if(!r.forstaBehTillfalle() || !r.sistaBehTillfalle())
					return;
				var forsta = RCC.Utils.parseYMD(r.forstaBehTillfalle());
				var sista = RCC.Utils.parseYMD(r.sistaBehTillfalle());
				if(!forsta || !sista)
					return;
				if(sista.getTime() < forsta.getTime())
					return { result: 'failed', message: 'Första rTMS skall vara före (eller samma som) sista rTMS', fatal: false };
			};

			r.forstaBehTillfalle.rcc.validation.add(r.forstaSistaRTMS);
			r.sistaBehTillfalle.rcc.validation.add(r.forstaSistaRTMS);

			addNumMinMaxValidation(r.antalBehTillfalle, 1, 999, false);
			addNumMinMaxValidation(r.motortroskel, 20, 90, false);
			addNumMinMaxValidation(r.sistaStyrka, 80, 150, false);
			addNumMinMaxValidation(r.dosTotBehDuration, 0, 200, false);
			addNumMinMaxValidation(r.dosTotBehDurationSekunder, 0, 59, false);

			isFutureDate(r.forstaBehTillfalle);
			isFutureDate(r.sistaBehTillfalle);
			isFutureDate(r.datifylld);
			isFutureDate(r.dummy);

			_.each(['sjukhus','datifylld','forstaBehTillfalle', 'sistaBehTillfalle','antalBehTillfalle'], function(name) { markAsFatal(r[name]); });
			
			r.foreMADRS.subscribe(function() {
				customSubTableMultiCondition(r.$$.ForeMADRSTotalpoang, r.foreMADRS, ['ja', 'ja_totalpoang']);
			});
			r.foreMADRSS.subscribe(function() {
				customSubTableMultiCondition(r.$$.ForeMADRSSTotalpoang, r.foreMADRSS, ['ja', 'ja_totalpoang']);
			});
			r.efterMADRS.subscribe(function() {
				customSubTableMultiCondition(r.$$.EfterMADRSTotalpoang, r.efterMADRS, ['ja', 'ja_totalpoang']);
			});
			r.efterMADRSS.subscribe(function() {
				customSubTableMultiCondition(r.$$.EfterMADRSSTotalpoang, r.efterMADRSS, ['ja', 'ja_totalpoang']);
			});


			/*r.dummy.rcc.validation.add(function(){
				if(!r.dummy() || r.$$.BehandlingsTillfalle().length == 0)
					return { result: 'failed', message: 'Värde saknas', fatal: true };
			});*/
		},
	
		Behandlingsnummer: function (r) {
			addNumMinMaxValidation(r.behandlingsnummer, 1, 999, false);
		},
		IndikationAnnan: function (r) {
			r.icd10Visible = ko.observable(false);
			r.icd10Text = ko.observable();
			r.icd10Picker = new ICD10.Picker({
				defaultChapter: 'Psykiska sjukdomar och syndrom samt beteendestörningar',
				addCode: function(code) {
					r.icd10Visible(false);
					r.r_indikationAnnan(code.id);
				}	
			});
			ko.computed(function() {
				var val = r.r_indikationAnnan();
				r.icd10Text(ICD10.informationForId(val).text());
			});
		},
		Uppfoljning: function(r) {
			if(inca.errand) {
				r.behandlingar = ko.observableArray(undefined);
				inca.form.getValueDomainValues({
					vdlist: 'VD_rtms_behandlingar',
					parameters: { patId: inca.form.env._PATIENTID },
					success: function (list) { r.behandlingar(list); }, 
					error: function (err) { console.log(err); } 
				});
			}
			markAsFatal(r.uppfDatifylld);
			isFutureDate(r.uppfDatifylld);
			if(inca.errand)
				markAsFatal(r.$vd.rtms_behandlingar);
		},
		Patientenkat: function (r) {
			ko.computed(function() {
				calculateTotalPoints([r.uppfMADRSSinnesstamning,r.uppfMADRSSOroskanslor,r.uppfMADRSSSomn,						
				r.uppfMADRSSMatlust,r.uppfMADRSSKoncformaga,r.uppfMADRSSInitiativformaga,r.uppfMADRSSKanslomassigtEng,					
				r.uppfMADRSSPessimism,r.uppfMADRSSLivslust], r.uppfMADRSSTotalpoang);
			});

			r.ovrigKommentar.rcc.validation.required(false);
		},
		ForeMADRSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.ForeMADRSDelsvar, p[0].foreMADRS, 'ja');
			foreRTMS(r.foreMADRSDatum, p[0].forstaBehTillfalle);
			isFutureDate(r.foreMADRSDatum);
		},
		ForeMADRSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([
					r.foreMADRSNedstamdhet,r.foreMADRSSAnktGrundstamning,r.foreMADRSAngestkanslor,r.foreMADRSMinskadNattsomn,							
					r.foreMADRSMinskadAptit,r.foreMADRSKoncsvarigheter,r.foreMADRSInitiativloshet,r.foreMADRSKanslomassigtEng,					
					r.foreMADRSDepTankeinnehall,r.foreMADRSSjalvmordstankar ], p[1].foreMADRSTotalpoang);
			});		
		},
		ForeMADRSSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.ForeMADRSSDelsvar, p[0].foreMADRSS, 'ja');
			foreRTMS(r.foreMADRSSDatum, p[0].forstaBehTillfalle);
			isFutureDate(r.foreMADRSSDatum);
		},
		ForeMADRSSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([r.foreMADRSSinnesstamning,			
					r.foreMADRSSOroskanslor,						
					r.foreMADRSSSomn,						
					r.foreMADRSSMatlust,							
					r.foreMADRSSKoncformaga,						
					r.foreMADRSSInitiativformaga,													
					r.foreMADRSSKanslomassigtEng,					
					r.foreMADRSSPessimism,							
					r.foreMADRSSLivslust ], p[1].foreMADRSSTotalpoang);
			});				
		},
		ForeEQ5D: function (r, p) {
			foreRTMS(r.foreEQ5DDatum, p[0].forstaBehTillfalle);
			isFutureDate(r.foreEQ5DDatum);
		},
		UnderMADRSS: function (r, p) {
			r.underMADRSS.subscribe(function() {
				customSubTableMultiCondition(r.$$.UnderMADRSSTotalpoang, r.underMADRSS, ['ja', 'ja_totalpoang']);
			});
		},
		UnderMADRSSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.UnderMADRSSDelsvar, p[1].underMADRSS, 'ja');
			underRTMS(r.underMADRSSDatum, p[0].forstaBehTillfalle, p[0].sistaBehTillfalle);
			isFutureDate(r.underMADRSSDatum);
		},
		UnderMADRSSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([r.underMADRSSinnesstamning,			
					r.underMADRSSOroskanslor,							
					r.underMADRSSSomn,						
					r.underMADRSSMatlust,							
					r.underMADRSSKoncformaga,						
					r.underMADRSSInitiativformaga,													
					r.underMADRSSKanslomassigtEng,					
					r.underMADRSSPessimism,							
					r.underMADRSSLivslust ], p[2].underMADRSSTotalpoang);
			});
		},
		UnderCGISkattning: function (r, p) {
			underRTMS(r.underCGISkattning, p[0].forstaBehTillfalle, p[0].sistaBehTillfalle);
			isFutureDate(r.underCGISkattning);
		},
		EfterMADRSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.EfterMADRSDelsvar, p[0].efterMADRS, 'ja');
			efterRTMS(r.efterMADRSDatum, p[0].sistaBehTillfalle);
			isFutureDate(r.efterMADRSDatum);
		},
		EfterMADRSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([
					r.efterMADRSNedstamdhet,r.efterMADRSSAnktGrundstamning,r.efterMADRSAngestkanslor,r.efterMADRSMinskadNattsomn,							
					r.efterMADRSMinskadAptit,r.efterMADRSKoncsvarigheter,r.efterMADRSInitiativloshet,r.efterMADRSKanslomassigtEng,					
					r.efterMADRSDepTankeinnehall,r.efterMADRSSjalvmordstankar ], p[1].efterMADRSTotalpoang);
			});		
		},
		EfterMADRSSTotalpoang: function (r, p) {
			customSubTableCondition(r.$$.EfterMADRSSDelsvar, p[0].efterMADRSS, 'ja');
			efterRTMS(r.efterMADRSSDatum, p[0].sistaBehTillfalle);
			isFutureDate(r.efterMADRSSDatum);
		},
		EfterMADRSSDelsvar: function (r, p) {
			ko.computed(function() {
				calculateTotalPoints([r.efterMADRSSinnesstamning,			
					r.efterMADRSSOroskanslor,						
					r.efterMADRSSSomn,						
					r.efterMADRSSMatlust,							
					r.efterMADRSSKoncformaga,						
					r.efterMADRSSInitiativformaga,													
					r.efterMADRSSKanslomassigtEng,					
					r.efterMADRSSPessimism,							
					r.efterMADRSSLivslust ], p[1].efterMADRSSTotalpoang);
			});				
		},
		EfterEQ5D: function (r, p) {
			efterRTMS(r.efterEQ5DDatum, p[0].sistaBehTillfalle);
			isFutureDate(r.efterEQ5DDatum);
		},
	};
})();

subTableConditions = new RCC.Vast.Utils.SubTableConditionHandler(
	eventHandlers.rowAdded,
	{
		r_indikation: [
			{ table: 'IndikationAnnan', value: 'annan' },
			{ table: 'IndikationFritext', value: 'fritext' },
		],
		suicidforsok: [
			{ table: 'Suicidforsok12man', value: ['1-2_ggr', 'ge_3_ggr'] }
		],
		foreEQ5D: [
			{ table: 'ForeEQ5D', value: 'ja' }
		],
		titrering: [
			{ table: 'Behandlingsnummer', value: 'ja' }
		],
		spoltyp: [
			{ table: 'SpoltypAnnat', value: 'annan' }
		],
		stimulator: [
			{ table: 'StimulatorAnnan', value: 'annan' }
		],
		thetaburstProtokoll: [
			{ table: 'ThetaburstProtokollAnnan', value: 'annan' }
		],
		lokalisation: [
			{ table: 'LokalisationAnnat', value: 'annat' }
		],
		lokalisationMetod: [
			{ table: 'LokalisationMetodAnnat', value: 'annat' }
		],
		motortroskelMetod: [
			{ table: 'MotortroskelMetodAnnat', value: 'annat' }
		],
		biverkanKomplikation: [
			{ table: 'BiverkanKomplikation', value: 'ja' }
		],
		biverkanKomplikationLista: [
			{ table: 'BiverkanKomplikationAnnan', value: 'annan' }
		],
		orsakAvslut: [
			{ table: 'OrsakAvslutAnnan', value: 'annan' }
		],
		efterEQ5D: [
			{ table: 'EfterEQ5D', value: 'ja' }
		],
		annanInfo: [
			{ table: 'AnnanInfo', value: 'ja' }
		],
		patientenkatBesvarad: [
			{ table: 'Patientenkat', value: 'ja' }
		],
		besvarRTMS: [
			{ table: 'Besvar', value: 'ja' }
		],
		kvarstaendeBiverkan: [
			{ table: 'KvarstaendeBiverkan', value: 'ja' }
		],
		underCGI: [
			{ table: 'UnderCGISkattning', value: function(value, row) {
				return value != 'ej_bedomt';
			}}
		]
	});

$(function () {

	var actions = { abort: ['Avbryt', 'Radera'], pause: ['Pausa'] };

	try {
		var vm = new RCC.ViewModel({
			validation: new RCC.Validation(),
			events: eventHandlers
		});

		// För offline.
		vm.showRegvarNames = ko.observable(window.showRegvarNames);

		var SendForm = function () {
			var errors = vm.$validation.errors(),
			selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			if (errors.length > 0) {
				vm.$validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);

				if(isPause) {
					return true;
				}

				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return true;
		};

		vm.pendingRequests = ko.observable(0);

		inca.on('validation', SendForm);
		// XXX: debug
		window.SendForm = SendForm;

		vm.errors = ko.observableArray([]);
		var ready = ko.observable(false);
		vm.showForm = ko.computed( function ( ) { return ready() && (vm.errors().length == 0 || vm.$form.isReadOnly) && vm.pendingRequests() == 0; } );

		vm.enableHelpText = ko.observable(true);
		vm.getSubTableConditionValue = subTableConditions.getConditionValue;
		vm.hasValue = function (observable, values) {
			var obsValue = observable();
			if (!obsValue) return false;
			if (typeof values == 'string') {
				values = [values];
			}
			return $.inArray(obsValue.value, values) != -1;
		};

		vm.isSavableRegisterRecord = !inca.errand && !inca.form.isReadOnly;
		

		if (!inca.errand) {
			vm.tabs = ko.observableArray([]);
			vm.activeTabIndex = ko.observable(0);

			vm.tabs.push({ template: 'behandling', data: vm, name: 'Behandling', tabdate: vm.datifylld });
			_.each(vm.$$.Uppfoljning(), function(row) {
				vm.tabs.push({ template: 'uppfoljning', data: row, name: 'Uppföljning', tabdate: row.uppfDatifylld });
			});
		}

		vm.removeRow = function (observableArray, row, name, event) {
			event.preventDefault();
			event.stopPropagation();

			if (confirm('Bekräfta borttag av "' + ko.unwrap(name) + '". Åtgärden går inte att ångra.')) {
				observableArray[RCC.metadataProperty].remove(row);

				if(vm.tabs) {
					_.each(vm.tabs(), function(tabRow) {
						if(row.uppfDatifylld() && row.uppfDatifylld() == tabRow.tabdate())
							vm.tabs.remove(tabRow);
					});	
				}
			
				if (vm.activeTabIndex() >= observableArray().length) {
					vm.activeTabIndex(vm.activeTabIndex() - 1);
				}	
			}
		};

		 _.each(['ICD10_Kapitel', 'ICD10_Kod', 'ICD10_Kapitelkoder'], function(s) {
		 	var vd = vm.$vd && vm.$vd[s];
		 	if (vd) vd.rcc.validation.required(false);
		});

		
		ko.applyBindings(vm);
		ready(true);

		// XXX: debug
		window.vm = vm;
	} catch (ex) {
		inca.on("validation", function () {
			if (inca.errand && inca.errand.action && $.inArray(inca.errand.action.find ? inca.errand.action.find(':selected').text() : undefined, actions.abort) != -1) return false;
		});
		alert("Ett fel inträffade\r\n\r\n" + ex + "\r\n" + JSON.stringify(ex));  
	}

});



